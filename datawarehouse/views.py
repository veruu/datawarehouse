# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Views file."""

import datetime
import json
import threading

from django.utils import timezone
from django.http import (JsonResponse, HttpResponse, HttpResponseRedirect,
                         HttpResponseNotFound, HttpResponseBadRequest, HttpResponseNotAllowed)
from django.template import loader
from django.db.models import Count, Q
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt

from . import models, cron, scripts, pagination, helpers


def _patch(request, context, series, template):
    page = request.GET.get('page')

    if context['group'] == 'tested':
        series = series.exclude(patches__pipelines=None).distinct()

    elif context['group'] == 'skipped':
        series = series.filter(patches__pipelines=None, skipped=True).distinct()

    elif context['group'] == 'missed':
        series = series.filter(patches__pipelines=None, skipped=False).distinct()

    paginator = pagination.EndlessPaginator(series, 30)
    context['patchseries'] = paginator.get_page(page)
    return HttpResponse(template.render(context, request))


def patch_get_by_submitter(request, group):
    """Return information of submitter's patches."""
    template = loader.get_template('web/patch_submitter.html')
    search = request.GET.get('search', '')
    context = {'group': group, 'search': search}

    if not search:
        context['emptypage'] = True
        return HttpResponse(template.render(context, request))

    submitter = models.PatchworkSubmitter.objects.filter(email__icontains=search).first()
    if submitter is None:
        return HttpResponseNotFound("Submitter not found")
    context['search'] = submitter.email
    series = submitter.patch_series.all()

    return _patch(request, context, series, template)


def patch_summary(request, group):
    """Return summary of patches."""
    template = loader.get_template('web/patch_summary.html')
    search = request.GET.get('search', '')
    context = {'group': group, 'search': search, 'showsubmitter': True}

    series = models.PatchworkSeries.objects.select_related('submitter')
    if search:
        series = series.filter(name__icontains=search)

    return _patch(request, context, series, template)


def pipeline_get(request, pipeline_id):
    """Get information of a pipeline."""
    template = loader.get_template('web/pipeline.html')

    try:
        pipe = (models.Pipeline.objects.add_stats().get(pipeline_id=pipeline_id))
    except models.Pipeline.DoesNotExist:
        return HttpResponseNotFound("Pipeline not found")

    test_jobs = (
        pipe.test_jobs
        .order_by('kernel_arch', 'beakertestrun__recipe_id', 'beakertestrun__task_id')
        .select_related()
        .prefetch_related('logs', 'beakertestrun__beaker_resource')
    )

    context = {
        'pipeline': pipe,
        'test_jobs': test_jobs,
        'issues': models.Issue.objects.filter(resolved=False).select_related('kind'),
        'issue_kinds': models.IssueKind.objects.all(),
        'issue_records': pipe.issue_records.all(),
        'failed_testruns': test_jobs.filter(passed=False)
    }
    return HttpResponse(template.render(context, request))


def pipeline_issue(request, pipeline_id):
    """Mark a pipeline's issue."""
    if request.method == "POST":
        issue_id = request.POST.get('issue_id')
        testrun_ids = request.POST.getlist('testrun_ids')

        helpers.create_issue_record(issue_id, pipeline_id, testrun_ids)

    return HttpResponseRedirect(f'/pipeline/{pipeline_id}')


def pipeline_issue_delete(request, pipeline_id, issue_record_id):
    """Delete IssueRecord from Pipeline.

    This should be done with a DELETE request but it's not available
    on HTML forms :'(
    """
    if request.method == "POST":
        models.IssueRecord.objects.get(id=issue_record_id).delete()

    return HttpResponseRedirect(f'/pipeline/{pipeline_id}')


def issue_action(request, action):
    """Perform {action} on issue."""
    if request.method == "POST":
        issue_id = request.POST.get('issue_id') or None
        description = request.POST.get('description')
        ticket_url = request.POST.get('ticket_url')
        kind_id = request.POST.get('kind_id')
        redirect_to = request.POST.get('redirect_to')
        resolved = request.POST.get('resolved') == 'on'
        generic = request.POST.get('generic') == 'on'

        if action in ('new', 'edit'):
            issue_kind = models.IssueKind.objects.get(id=kind_id)
            if models.Issue.objects.exclude(id=issue_id).filter(ticket_url=ticket_url).exists():
                return HttpResponseBadRequest(f'Issue already exists with ticket URL {ticket_url}')

        if action == 'new':
            models.Issue.objects.create(
                description=description,
                ticket_url=ticket_url,
                kind=issue_kind,
                generic=generic,
            )

        elif action == 'edit':
            models.Issue.objects.filter(id=issue_id).update(
                description=description,
                ticket_url=ticket_url,
                kind=issue_kind,
                resolved=resolved,
                generic=generic,
            )

        elif action == 'resolve':
            models.Issue.objects.filter(id=issue_id).update(
                resolved=resolved,
            )

        else:
            return HttpResponseBadRequest(f'Heh, no idea how to do {action}.')

        return HttpResponseRedirect(redirect_to)

    return HttpResponseNotAllowed(['POST'])


def cron_run(request):
    """Run cron tasks to fetch new data."""
    thread = threading.Thread(target=cron.run)
    thread.setDaemon(True)
    thread.start()

    return JsonResponse({})


def bootstrap(request):
    """Initialize an empty projects table."""
    return JsonResponse(scripts.bootstrap_projects())


def dashboard(request):
    """Show dashboard."""
    template = loader.get_template('web/dashboard.html')
    page = request.GET.get('page')

    date_from = timezone.now() - datetime.timedelta(days=1)

    pipelines = models.Pipeline.objects.add_stats()
    last_24_hs = pipelines.filter(created_at__gte=date_from)

    build_last_24_hs = models.BuildRun.objects.filter(pipeline__created_at__gte=date_from)
    build_last_24_hs_success = build_last_24_hs.filter(success=True)
    build_last_24_hs_fail = build_last_24_hs.filter(success=False)

    test_last_24_hs = models.TestRun.objects.filter(pipeline__created_at__gte=date_from)
    test_last_24_hs_success = test_last_24_hs.filter(passed=True)
    test_last_24_hs_fail = test_last_24_hs.exclude(passed=True)

    git_trees = models.GitTree.objects.filter(pipeline__created_at__gte=date_from)\
                                      .select_related().annotate(pipelines=Count('pipeline'))

    paginator = pagination.EndlessPaginator(pipelines, 30)
    context = {
        'pipelines': paginator.get_page(page),
        'pipelines_last_24_hs': last_24_hs,
        'build_last_24_hs': build_last_24_hs,
        'build_last_24_hs_fail': build_last_24_hs_fail,
        'build_last_24_hs_success': build_last_24_hs_success,
        'test_last_24_hs': test_last_24_hs,
        'test_last_24_hs_fail': test_last_24_hs_fail,
        'test_last_24_hs_success': test_last_24_hs_success,
        'git_trees': git_trees,
    }

    return HttpResponse(template.render(context, request))


def pipeline_submit(request, project_id, pipeline_id):
    """Submit a new pipeline."""
    thread = threading.Thread(target=scripts.submit_pipeline, args=[project_id, pipeline_id])
    thread.setDaemon(True)
    thread.start()

    return HttpResponse("Pipeline saved correctly")


@csrf_exempt
def pipeline_submit_webhook(request):
    """Webhook for gitlab to submit a new pipeline."""
    body = json.loads(request.body.decode('utf-8'))
    kind = body.get("object_kind")

    if kind != 'pipeline':
        return HttpResponseBadRequest('Only Pipeline webhook is supported')

    pipeline_id = body.get("object_attributes").get("id")
    project_id = body.get("project").get("id")

    thread = threading.Thread(target=scripts.submit_pipeline, args=[project_id, pipeline_id])
    thread.setDaemon(True)
    thread.start()

    return HttpResponse("Pipeline saved correctly")


def pipeline_check(request, project_id, pipeline_id):
    """Check if pipeline's data is healthy."""
    try:
        scripts.check_pipeline(project_id, pipeline_id)
    except ObjectDoesNotExist:
        return HttpResponseNotFound("Pipeline does not look correct.")

    return HttpResponse("Everything looks good.")


def pipeline_check_all(request):
    """Check all pipelines' data health since 'since_days' days."""
    since_days = request.POST.get('since_days', 1)

    thread = threading.Thread(target=scripts.check_and_fix_pipelines, args=[since_days])
    thread.setDaemon(True)
    thread.start()

    return HttpResponse(f"All pipelines since {since_days} day(s) ago are being updated on background.")


def confidence(request, group):
    # pylint: disable=too-many-locals, too-many-branches
    """Confidence dashboard. Can be by tests or hosts."""
    template = loader.get_template('web/confidence.html')
    days_ago = request.GET.get('days_ago', 7)
    search = request.GET.get('search')

    if days_ago == 'ever':
        date_from = timezone.make_aware(datetime.datetime.min)
    else:
        date_from = timezone.now() - datetime.timedelta(days=int(days_ago))

    if group == 'tests':
        group_item = models.Test
        related_item = 'testrun'
    elif group == 'hosts':
        group_item = models.BeakerResource
        related_item = 'beakertestrun'
    else:
        return HttpResponseNotFound("Not implemented. group should be 'tests' or 'hosts'")

    results = models.TestResult.objects.all()

    all_items = group_item.objects.filter(**{f'{related_item}__pipeline__created_at__gte': date_from})

    if search:
        if group == 'tests':
            all_items = all_items.filter(name__icontains=search)
        elif group == 'hosts':
            all_items = all_items.filter(fqdn__icontains=search)

    for result in results:
        all_items = all_items.annotate(
            **{result.name: Count(related_item, filter=Q(**{f'{related_item}__result__name': result.name,
                                                            f'{related_item}__invalid_result': False}))})

    all_items = all_items.annotate(total=Count(related_item, filter=Q(**{f'{related_item}__invalid_result': False})))

    items_map = []
    for item in all_items:

        test_results = {}
        test_results_percent = {}

        if not item.total:
            continue

        for result in results:
            count = getattr(item, result.name)
            test_results[result.name] = count
            test_results_percent[result.name] = 100 / item.total * count

        confidence_index = item.PASS / item.total

        items_map.append({
            'item': item,
            'confidence': confidence_index,
            'results': test_results,
            'results_percent': test_results_percent,
            'total': item.total})

    items_map = sorted(items_map, key=lambda t: t.get('confidence'))

    context = {'map': items_map, 'group': group, 'since': days_ago, 'search': search}
    return HttpResponse(template.render(context, request))


def details(request, group, item_id):
    # pylint: disable=too-many-locals
    """Show testsruns by test."""
    template = loader.get_template('web/details.html')
    page = request.GET.get('page')
    result_filter = request.GET.get('result')

    if group == 'test':
        item = models.Test.objects.get(id=item_id)
        related_name = 'beakertestrun'
        table_by = models.BeakerResource
        testrun_to_item = 'test'

    elif group == 'host':
        item = models.BeakerResource.objects.get(id=item_id)
        related_name = 'testrun'
        table_by = models.Test
        testrun_to_item = 'beakertestrun__beaker_resource'

    else:
        return HttpResponseNotFound("Not implemented. group should be 'test' or 'host'")

    pipelines = models.Pipeline.objects.filter(**{f'test_jobs__{testrun_to_item}': item,
                                                  'test_jobs__invalid_result': False}).distinct()

    table_by = table_by.objects.filter(**{f'{related_name}__{testrun_to_item}': item,
                                          f'{related_name}__invalid_result': False})\
                               .annotate(total_runs=Count(related_name))\
                               .order_by('-total_runs')

    if result_filter:
        pipelines = pipelines.filter(
            **{f'test_jobs__{testrun_to_item}': item, 'test_jobs__result__name': result_filter})
        table_by = table_by.filter(
            **{f'{related_name}__{testrun_to_item}': item, f'{related_name}__result__name': result_filter})

    runs_list = []

    for pipeline in pipelines:
        runs = pipeline.test_jobs.filter(**{testrun_to_item: item,
                                            'invalid_result': False}).select_related()

        if result_filter:
            runs = runs.filter(result__name=result_filter)

        runs_list.append({
            'pipeline': pipeline,
            'tests': runs,
        })

    for result in models.TestResult.objects.all():
        table_by = table_by.annotate(
            **{result.name: Count(related_name, filter=Q(**{f'{related_name}__result': result}))}
        )

    paginator = pagination.EndlessPaginator(runs_list, 30)
    context = {
        'item': item,
        'runs': paginator.get_page(page),
        'type': group,
        'results': models.TestResult.objects.all(),
        'result_filter': result_filter,
        'table': table_by
    }

    return HttpResponse(template.render(context, request))


def pipelines_by_failure(request, group):
    """Show failed pipelines classified by stage."""
    template = loader.get_template('web/failures.html')
    page = request.GET.get('page')

    stages = models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test'])
    issues = helpers.get_failed_pipelines(group)

    paginator = pagination.EndlessPaginator(issues, 30)
    context = {
        'stages': stages,
        'pipelines': paginator.get_page(page),
        'group': group
    }

    return HttpResponse(template.render(context, request))


def pipelines_running(request):
    """Get information of running pipelines."""
    template = loader.get_template('web/running.html')
    page = request.GET.get('page')

    date_from = timezone.now() - datetime.timedelta(days=7)

    pipes = models.Pipeline.objects.filter(duration=None).filter(created_at__gte=date_from)

    paginator = pagination.EndlessPaginator(pipes, 30)
    context = {'pipelines': paginator.get_page(page)}
    return HttpResponse(template.render(context, request))


def issue_list(request, group):
    """Get all issues."""
    template = loader.get_template('web/issues_list.html')
    page = request.GET.get('page')
    search = request.GET.get('search', '')

    issues = (models.Issue.objects
              .filter(resolved=group == 'resolved')
              .select_related('kind')
              .prefetch_related(
                  'issue_records',
                  'issue_records__pipeline',
                  'issue_records__pipeline__gittree',
                  'issue_records__pipeline__lint_jobs',
                  'issue_records__pipeline__merge_jobs',
                  'issue_records__pipeline__build_jobs',
                  'issue_records__pipeline__test_jobs',
                  'issue_records__pipeline__test_jobs__result',
              ))

    if search:
        issues = issues.filter(description__icontains=search)

    paginator = pagination.EndlessPaginator(issues, 10)

    context = {
        'issues': paginator.get_page(page),
        'issue_kinds': models.IssueKind.objects.all(),
        'search': search,
        'group': group,
    }

    return HttpResponse(template.render(context, request))


def pipeline_summary(request, tree):
    """Return summary of pipelines."""
    template = loader.get_template('web/pipeline_summary.html')
    page = request.GET.get('page')

    pipelines = models.Pipeline.objects.add_stats()

    if tree != 'all':
        pipelines = pipelines.filter(gittree__name=tree)

    paginator = pagination.EndlessPaginator(pipelines, 30)
    context = {
        'pipelines': paginator.get_page(page),
        'selected_tree': tree,
        'trees': models.GitTree.objects.all().order_by('name'),
    }

    return HttpResponse(template.render(context, request))


def metrics(request, tree):
    """Get some metrics."""
    template = loader.get_template('web/metrics.html')
    parameter_from = request.GET.get('from', 'all')
    days_filter = {
        'all': 9999,
        '3 months': 90,
        '1 month': 30,
        '2 weeks': 15,
        '1 week': 7,
    }

    date_from = timezone.now() - timezone.timedelta(days=days_filter[parameter_from])

    if tree == 'all':
        trees_list = models.GitTree.objects.all()
    else:
        trees_list = models.GitTree.objects.filter(name=tree)

    groups = [
        scripts.get_series_metrics(trees_list, date_from),
        scripts.get_pipelines_metrics(trees_list, date_from),
        scripts.get_tests_metrics(trees_list, date_from),
        ]

    context = {
        'tree': tree,
        'trees': models.GitTree.objects.all().order_by('name'),
        'groups': groups,
        'days_filter_available':  days_filter,
        'days_filter': parameter_from,
    }

    return HttpResponse(template.render(context, request))
