"""Umb reports module."""
import base64
import os
import json
import lzma
import requests
import jsonschema

from django.core.files.base import ContentFile
from cki_lib.retrying import retrying_on_exception
from datawarehouse import models


def validate_schema(data):
    """Validate the data against the expected schema."""
    schema = {
        "type": "object",
        "properties": {
            "test_name": {"type": "string"},
            "test_description": {"type": "string"},
            "test_result": {"type": "string", "pattern": "^(PASS|FAIL)$"},
            "test_log_url": {"type": "string"},
            "test_arch": {"type": "string"},
            "test_waived": {"type": "string", "pattern": "^(True|False)$"},
        },
        "required": ["test_name", "test_description", "test_result", "test_log_url",
                     "test_arch", "test_waived"]
    }
    jsonschema.validate(data, schema)


@retrying_on_exception(Exception, initial_delay=10)
def download_file(url):
    """Download a file and handle retries."""
    return requests.get(url)


def download_log_to_testrun(test_run, log_url):
    """Download log_url and add it to test_run."""
    file_name = os.path.basename(log_url)
    log = models.Artifact.objects.create(
        name=file_name,
        pipeline=test_run.pipeline,
    )

    response = download_file(log_url)
    log.file.save(f'{test_run.kernel_arch.name}_ext_{test_run.test.name_sanitized}_{file_name}',
                  ContentFile(response.content))
    log.save()
    test_run.logs.add(log)


def create_test_run(pipeline, test_run_data):
    """Create TestRun instance from test_run data received."""
    test_name = '{test_name}: {test_description}'.format(**test_run_data)
    test, _ = models.Test.objects.get_or_create(
        name=test_name,
    )

    result = models.TestResult.objects.get(name=test_run_data['test_result'])
    passed = result.name == 'PASS'
    architecture, _ = models.Architecture.objects.get_or_create(name=test_run_data['test_arch'])
    waived = test_run_data['test_waived'] == 'True'

    test_run = models.TestRun.objects.create(
        pipeline=pipeline,
        test=test,
        result=result,
        waived=waived,
        kernel_arch=architecture,
        passed=passed,
    )

    download_log_to_testrun(test_run, test_run_data['test_log_url'])


def process_umb_result(gitlab_pipeline):
    """Process pipeline for umb-results."""
    variables = {var.key: var.value for var in gitlab_pipeline.variables.list()}
    pipeline = models.Pipeline.objects.get(pipeline_id=variables['original_pipeline_id'])

    testrun_list = json.loads(lzma.decompress(base64.b64decode(variables['data'])).decode())

    for test_run in testrun_list:
        validate_schema(test_run)
        create_test_run(pipeline, test_run)
