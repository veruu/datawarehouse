"""Django settings for datawarehouse project."""
import os
from socket import gethostname, gethostbyname
from urllib.parse import urlparse

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

# pylint: disable=wildcard-import, unused-wildcard-import
from datawarehouse.settings_s3 import *  # noqa: F401, F403

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = os.getenv('SECRET_KEY')

ALLOW_DEBUG_PIPELINES = bool(os.environ.get('ALLOW_DEBUG_PIPELINES', False))
IS_PRODUCTION = bool(os.environ.get('IS_PRODUCTION', False))
CSRF_COOKIE_SECURE = IS_PRODUCTION
CORS_ORIGIN_ALLOW_ALL = IS_PRODUCTION
DEBUG = not IS_PRODUCTION
SESSION_COOKIE_SECURE = IS_PRODUCTION

BEAKER_URL = os.environ.get('BEAKER_URL')
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', '')
GITLAB_URL = os.getenv('GITLAB_URL')
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")
PATCHWORK_URL = os.getenv('PATCHWORK_URL')

REQUESTS_MAX_WORKERS = os.environ.get("REQUESTS_MAX_WORKERS", 10)
REQUESTS_MAX_RETRIES = os.environ.get("REQUESTS_MAX_RETRIES", 3)
REQUESTS_TIMEOUT_S = os.environ.get("REQUESTS_TIMEOUT_S", 60)
REQUESTS_TIME_BETWEEN_RETRIES_S = os.environ.get("REQUESTS_TIME_BETWEEN_RETRIES_S", 2)  # noqa

ALLOWED_HOSTS = [
    urlparse(os.getenv('DATAWAREHOUSE_URL', 'http://*')).netloc,
    gethostname(),
    gethostbyname(gethostname()),
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'datawarehouse',
    'rest_framework',
    'rest_framework.authtoken',
    'django_extensions',
    'corsheaders',
    'django_cleanup.apps.CleanupConfig',
    'debug_toolbar',
    'django_s3_storage',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'datawarehouse.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'datawarehouse.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'datawarehouse',
        'USER': 'datawarehouse',
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': 5432,
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = '/code/nginx/static'

MEDIA_PATH = '/staticfiles/'
MEDIA_URL = f'{DATAWAREHOUSE_URL}{MEDIA_PATH}'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')

if IS_PRODUCTION:
    # the DSN is read from SENTRY_DSN
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[DjangoIntegration()]
    )


def show_toolbar(request):
    """Show the toolbar when requested with the debug parameter."""
    return (not request.is_ajax() and
            (request.GET.get('debug') or request.path.startswith('/__debug__/')))


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': 'datawarehouse.settings.show_toolbar'}

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
    ]
}

SHELL_PLUS_PRINT_SQL_TRUNCATE = 10000
