# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2019 Red Hat, Inc.
"""Metrics file."""
from django.db.models import Count, Q, F
from django.db.models.functions import TruncWeek

from datawarehouse import models


HOVER_COLORS = ['#A13647', '#256E5E', '#AA6439', '#452F74',
                '#4A9130', '#AA9A39', '#452F74', '#AA4139',
                '#2E4172', '#EA9739', '#277650', '#41837E',
                ]
COLORS = [f'{color}50' for color in HOVER_COLORS]


def get_series_metrics(trees_list, date_from):
    """Generate metrics plot for series."""
    pipelines_targeted = models.TestRun.objects.filter(pipeline__gittree__in=trees_list,
                                                       targeted=True).values('pipeline_id')

    filter_series_targeted = Count('patches', filter=Q(patches__pipelines__id__in=pipelines_targeted))
    series = (models.PatchworkSeries.objects
              .filter(patches__pipelines__gittree__in=trees_list, submitted_date__gte=date_from)
              .annotate(targeted=filter_series_targeted))

    series_targeted = series.filter(targeted__gt=0)
    series_targeted_id = series_targeted.values('series_id')

    series_by_date = (
        series
        .annotate(date=TruncWeek('submitted_date'))
        .order_by('date')
        .values('date')
        .annotate(**{
            'total': Count('series_id', distinct=True),
            'total_targeted': Count('series_id', filter=Q(series_id__in=series_targeted_id), distinct=True),
            'targeted_percent': F('total_targeted')*100/F('total'),
            })
    )

    plots = [
        {'type': 'area', 'title': 'Total / Targeted patch series', 'element_id': 'plot_1',
         'values': series_by_date,
         'x_axis': 'date',
         'y_axis': [
             {'id': 'A', 'label': '# Pipelines', 'position': 'left', 'lines': [
                 {'label': 'Targeted #', 'value': 'total_targeted',
                  'bg_color': '#ac880a10', 'border_color': '#ac880a15'},
                 {'label': 'Total #', 'value': 'total',
                  'bg_color': '#2a188a10', 'border_color': '#2a188a15'},
                 ]
              },
             {'id': 'B', 'label': 'Percent', 'position': 'right', 'append': '%', 'lines': [
                 {'label': 'Targeted %', 'value': 'targeted_percent',
                  'bg_color': '#00000000', 'border_color': '#1cc88a'},
                 ]
              }
             ]
         }
    ]

    return {'title': 'Series Details', 'plots': plots}


def get_pipelines_metrics(trees_list, date_from):
    """Generate metrics plot for pipelines."""
    issues = (models.Pipeline.objects
              .filter(gittree__in=trees_list, created_at__gte=date_from)
              .aggregate(**{
                  'success': Count('pipeline_id', filter=(
                      Q(lint_jobs__success=True) &
                      Q(merge_jobs__success=True) &
                      Q(build_jobs__success=True) &
                      Q(test_jobs__passed=True, test_jobs__untrusted=False)), distinct=True),
                  'lint_issues': Count('pipeline_id', filter=Q(lint_jobs__success=False), distinct=True),
                  'merge_issues': Count('pipeline_id', filter=Q(merge_jobs__success=False), distinct=True),
                  'build_issues': Count('pipeline_id', filter=Q(build_jobs__success=False), distinct=True),
                  'test_issues': Count('pipeline_id', filter=Q(test_jobs__passed=False), distinct=True),
                  })
              )

    plots = [
        {'type': 'doughnut', 'title': 'Pipeline Issues', 'element_id': 'plot_2', 'size': 12,
         'labels': ['Lint', 'Merge', 'Build', 'Test', 'Passed'],
         'values': [issues['lint_issues'], issues['merge_issues'], issues['build_issues'],
                    issues['test_issues'], issues['success']],
         'color': COLORS,
         'hover_color': HOVER_COLORS,
         'description': 'Distribution of failures through pipeline stages.'
         }
        ]
    return {'title': 'Pipelines Details', 'plots': plots}


def get_tests_metrics(trees_list, date_from):
    """Generate metrics plot for tests."""
    failed_testruns = models.Pipeline.objects.filter(
        gittree__in=trees_list,
        created_at__gte=date_from,
        test_jobs__passed=False,
        test_jobs__untrusted=False)

    aggregate = {
        'Not Tagged': Count('pipeline_id', filter=Q(issue_records=None), distinct=True)
    }
    for issue in models.IssueKind.objects.all():
        aggregate.update({
            issue.description: Count('pipeline_id', filter=Q(issue_records__issue__kind=issue), distinct=True)
        })

    failed_testruns = failed_testruns.aggregate(**aggregate)

    plots = [
        {'type': 'doughnut', 'title': 'Issue Records', 'element_id': 'plot_4',
         'labels': list(failed_testruns.keys()),
         'values': list(failed_testruns.values()),
         'color': ['#00000010']+COLORS,
         'hover_color': ['#00000040']+HOVER_COLORS,
         'description': (
             'Number of failed pipelines categorized by issue kind. ' +
             'Where multiple issue records exist for a pipeline.'
         ),
         },
        ]

    # Usual test issues.
    tests = (
        models.Test.objects
        .annotate(**{
            'issues': Count('testrun', filter=Q(
                testrun__passed=False,
                testrun__untrusted=False,
                testrun__pipeline__created_at__gte=date_from,
            )),
            })
        .order_by('-issues')
        )[:10]

    plots.extend([
        {'type': 'doughnut', 'title': 'Top Failed Tests', 'element_id': 'plot_5',
         'labels': [test.name for test in tests],
         'values': [test.issues for test in tests],
         'color': COLORS,
         'hover_color': HOVER_COLORS,
         'description': (
             'Number of failed pipelines categorized by test. ' +
             'Where multiple tests failed for a pipeline.'
         ),
         }
        ])

    # Usual architecture issues.
    architectures = (
        models.Architecture.objects
        .exclude(name='generic')
        .annotate(**{
            'issues': Count('testrun', filter=Q(
                testrun__passed=False,
                testrun__untrusted=False,
                testrun__pipeline__created_at__gte=date_from,
            )),
            })
        .order_by('-issues')
        )

    plots.extend([
        {'type': 'doughnut', 'title': 'Top Failed Architectures', 'element_id': 'plot_6',
         'labels': [architecture.name for architecture in architectures],
         'values': [architecture.issues for architecture in architectures],
         'color': COLORS,
         'hover_color': HOVER_COLORS,
         'description': (
             'Number of failed pipelines categorized by architecture. ' +
             'Where multiple architectures failed for a pipeline.'
         ),
         }
        ])

    # Unique issues.
    issues = models.Issue.objects.filter(
        issue_records__pipeline__gittree__in=trees_list,
        issue_records__pipeline__created_at__gte=date_from,
    ).distinct()

    issue_kinds = (
        models.IssueKind.objects
        .annotate(
            issues_count=Count('issue', filter=Q(issue__in=issues), distinct=True),
            generic_issues_reports_count=Count(
                'issue__issue_records',
                filter=(
                    Q(issue__in=issues) &
                    Q(issue__generic=True) &
                    Q(issue__issue_records__pipeline__created_at__gte=date_from)
                ),
                distinct=True),
            generic_issues_count=Count(
                'issue',
                filter=(
                    Q(issue__in=issues) &
                    Q(issue__generic=True)
                ),
                distinct=True),
        )
    )

    values = {}
    for issue_kind in issue_kinds:
        values[issue_kind.description] = (
            issue_kind.issues_count
            + issue_kind.generic_issues_reports_count
            - issue_kind.generic_issues_count
        )

    plots.extend([
        {'type': 'doughnut', 'title': 'Unique Issues per Kind', 'element_id': 'plot_unique_issues',
         'labels': list(values.keys()),
         'values': list(values.values()),
         'color': COLORS,
         'hover_color': HOVER_COLORS,
         'description': 'Note: Records from generic Issues are counted individually.'
         }
        ])

    # Generic issues.
    generic_issues = models.Issue.objects.filter(
        issue_records__pipeline__gittree__in=trees_list,
        issue_records__pipeline__created_at__gte=date_from,
        generic=True,
    ).annotate(
        count_records=Count('issue_records')
    )

    plots.extend([
        {'type': 'doughnut', 'title': 'Generic Issues Records', 'element_id': 'plot_generics',
         'labels': [f'[{issue.kind.tag}] {issue.description}' for issue in generic_issues],
         'values': [issue.count_records for issue in generic_issues],
         'color': COLORS,
         'hover_color': HOVER_COLORS,
         'description': 'Number of Issue Records per Generic Issue.',
         },
    ])

    return {'title': 'Test Issue Details', 'plots': plots}
