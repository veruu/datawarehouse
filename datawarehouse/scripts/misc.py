# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
import datetime
from distutils.util import strtobool
import logging
import re

from django.core.files.base import ContentFile
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from sentry_sdk import capture_exception

from cki_lib import gitlab

from datawarehouse import models
from datawarehouse.beaker import Beaker
from datawarehouse.logs_publisher import BeakerLogsDownloader
from datawarehouse.rc import get_rcs
from datawarehouse.umb import process_umb_result
from datawarehouse.utils import parse_patches_from_urls, timestamp_to_datetime
from datawarehouse.patches import store_patchwork_series_patches


logger = logging.getLogger(__name__)


def copy_variables(pipeline, g_variables):
    """Create a TriggerVariable for each trigger variable."""
    for variable in g_variables:
        models.TriggerVariable.objects.create(
            key=variable.attributes.get('key'),
            value=variable.attributes.get('value'),
            pipeline=pipeline,
        )


def copy_patches(pipeline, patch_urls):
    """Create the patches for the pipeline according to cki_pipeline_type."""
    if pipeline.trigger_variables.get('cki_pipeline_type') == 'patchwork':
        for patch in store_patchwork_series_patches(patch_urls):
            pipeline.patches.add(patch)
    else:
        for patch_info in parse_patches_from_urls(patch_urls):
            patch, _ = models.Patch.objects.get_or_create(**patch_info)
            pipeline.patches.add(patch)


def copy_log(pipeline, name, content):
    """Return a Artifact instance after saving it with content."""
    if not content:
        return None

    log = models.Artifact.objects.create(
        name=name,
        pipeline=pipeline,
    )

    log.file.save(name, ContentFile(content))
    log.save()

    return log


def is_retrigger(job_data, g_variables):
    """Decide if the pipeline is a retrigger."""
    return (any(strtobool(v.value) for v in g_variables if v.key == 'retrigger') or
            job_data.commit.get('message', '').startswith('Retrigger'))


def should_ignore_pipeline(job_data, g_variables):
    """Decide if the pipeline should be skipped."""
    if settings.ALLOW_DEBUG_PIPELINES:
        return False

    # Retrigger pipelines are meant as CI for our tools.
    if is_retrigger(job_data, g_variables):
        return True

    return False


def update_invalid_results(recipe_id):
    """Mark every result after a Cancelled/Aborted task as invalid."""
    runs = models.BeakerTestRun.objects.filter(recipe_id=recipe_id).order_by('-task_id')\
                                       .select_related('beaker_status')
    if runs.count() == 0:
        return
    last_status = runs.first().beaker_status.name
    if last_status not in ('Cancelled', 'Aborted'):
        return
    same_status_task_ids = []
    for run in runs:
        if run.beaker_status.name == last_status:
            same_status_task_ids.append(run.task_id)
        else:
            break
    for run in runs:
        run.invalid_result = run.task_id in same_status_task_ids[:-1]
        run.save()


def submit_pipeline(project_id, pipeline_id):
    """Submit a new pipeline to the datawarehouse."""
    # pylint: disable=too-many-locals, too-many-nested-blocks, too-many-branches, too-many-statements
    beaker = Beaker(settings.BEAKER_URL)
    gitlab_helper = gitlab.GitlabHelper(settings.GITLAB_URL, settings.GITLAB_TOKEN)
    gitlab_helper.set_project(project_id)

    g_pipeline = gitlab_helper.project.pipelines.get(pipeline_id)
    g_variables = g_pipeline.variables.list(all=True)

    if g_pipeline.ref == 'umb-results':
        process_umb_result(g_pipeline)
        return

    for job_data, rc_data in get_rcs(project_id, pipeline_id):

        if should_ignore_pipeline(job_data, g_variables):
            logger.info("Ignoring pipeline. (project_id %d, pipeline_id %d)", project_id, pipeline_id)
            return

        stage, _ = models.Stage.objects.get_or_create(name=job_data.stage)
        job, _ = models.Job.objects.get_or_create(name=job_data.name, stage=stage)

        project, _ = models.Project.objects.get_or_create(
            project_id=project_id,
            path=gitlab_helper.project.attributes.get('path_with_namespace')
        )

        pipeline, created = models.Pipeline.objects.get_or_create(
            commit_id=job_data.commit.get('id'),
            pipeline_id=pipeline_id,
            project=project
        )

        if created:
            copy_variables(pipeline, g_variables)

            # If there are patches applied, link them to the pipeline now.
            try:
                patch_urls = pipeline.variables.get(key='patch_urls').value.split(' ')
            except models.TriggerVariable.DoesNotExist:
                pass
            else:
                copy_patches(pipeline, patch_urls)

        # Update pipeline with the new data available
        pipeline.kernel_type = rc_data.state.get('kernel_type')
        pipeline.test_hash = rc_data.state.get('test_hash')
        pipeline.tag = rc_data.state.get('tag')
        pipeline.make_target = rc_data.state.get('make_target')
        pipeline.kernel_version = rc_data.state.get('kernel_version')
        pipeline.commit_message_title = rc_data.state.get('commit_message_title')

        pipeline.created_at = g_pipeline.attributes.get('created_at')
        pipeline.started_at = g_pipeline.attributes.get('started_at')
        pipeline.duration = g_pipeline.attributes.get('duration')
        pipeline.finished_at = g_pipeline.attributes.get('finished_at')

        gittree_name = g_pipeline.attributes.get('ref')
        # If we accept retrigger pipelines, group them under a single tree name
        if is_retrigger(job_data, g_variables):
            gittree_name = "retriggers"
        gittree, _ = models.GitTree.objects.get_or_create(name=gittree_name)
        pipeline.gittree = gittree

        pipeline.save()

        if job_data.stage == 'lint':
            # Process lint stages

            # Try to delete retried jobs.
            pipeline.lint_jobs.exclude(job=job).filter(job__name=job.name).delete()

            if models.LintRun.objects.filter(jid=job_data.id).exists():
                continue

            success = rc_data.state.get('stage_lint') == 'pass'

            log = copy_log(pipeline, f'lint_{job.name}.log',
                           gitlab_helper.get_artifact(job_data.id, 'lint.log', raw=True))

            models.LintRun.objects.create(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
                success=success,
                command=rc_data.state.get('lintcmd'),
                log=log,
            )

        elif job_data.stage == 'merge':
            # Create the MergeRun instance

            # Try to delete retried jobs.
            pipeline.merge_jobs.exclude(job=job).filter(job__name=job.name).delete()

            if models.MergeRun.objects.filter(jid=job_data.id).exists():
                continue

            success = rc_data.state.get('stage_merge') == 'pass'

            log = copy_log(pipeline, f'merge.log',
                           gitlab_helper.get_artifact(job_data.id, 'merge.log', raw=True))

            models.MergeRun.objects.create(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
                success=success,
                log=log,
            )

            # If there's a stage_build, we're building SRPM here too.
            # Add the SRPM build as another build job.
            if rc_data.state.get('stage_build'):
                architecture, _ = models.Architecture.objects.get_or_create(name="generic")
                success = rc_data.state.get('stage_build') == 'pass'
                build_duration = None
                if rc_data.state.get('srpm_build_time_end'):
                    build_duration = (
                        rc_data.state.get('srpm_build_time_end') -
                        rc_data.state.get('srpm_build_time_start'))
                build = models.BuildRun.objects.create(
                    job=job,
                    jid=job_data.id,
                    pipeline=pipeline,
                    kernel_arch=architecture,
                    success=success,
                    make_opts='Build SRPM',
                    duration=build_duration
                )

        elif job_data.stage == 'build':
            # Add the build to the pipeline.
            # We only get one rc per build.

            # Try to delete retried jobs.
            pipeline.build_jobs.exclude(job=job).filter(job__name=job.name).delete()

            if models.BuildRun.objects.filter(jid=job_data.id).exists():
                continue

            architecture, _ = models.Architecture.objects.get_or_create(name=rc_data.state.get('kernel_arch'))
            success = rc_data.state.get('stage_build') == 'pass'

            log = copy_log(pipeline, f'build_{architecture.name}.log',
                           gitlab_helper.get_artifact(job_data.id, 'build.log', raw=True))

            build_duration = None
            if rc_data.state.get('build_time_end'):
                build_duration = (rc_data.state.get('build_time_end') -
                                  rc_data.state.get('build_time_start'))
            if rc_data.state.get('compiler'):
                compiler, _ = models.Compiler.objects.get_or_create(name=rc_data.state.get('compiler'))
            else:
                compiler = None
            build = models.BuildRun.objects.create(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
                success=success,
                kernel_arch=architecture,
                make_opts=rc_data.state.get('make_opts'),
                url=rc_data.state.get('build_job_url'),
                repo_path=rc_data.state.get('repo_path'),
                log=log,
                duration=build_duration,
                compiler=compiler,
            )

            if not pipeline.gittree.internal:
                artifacts_filenames = []
                if success:
                    artifacts_filenames.extend([
                        rc_data.state['tarball_file'],
                        rc_data.state['config_file'],
                    ])

                if pipeline.trigger_variables.get('build_selftests'):
                    artifacts_filenames.extend([
                        'kselftest.tar.gz',
                        'build-selftests.log'
                    ])

                for filename in artifacts_filenames:
                    artifact = models.Artifact.objects.create(
                        name=filename,
                        pipeline=pipeline,
                    )
                    content = gitlab_helper.get_artifact(job_data.id, filename, raw=True)
                    artifact.file.save(filename, ContentFile(content))
                    artifact.save()
                    build.artifacts.add(artifact)

        elif job_data.stage == 'publish':
            if project.path == 'cki-project/brew-pipeline':
                # rhel5 brew pipelines have a publish stage, but no build.
                # Ignore publish stages on brew pipelines.
                continue

            architecture, _ = models.Architecture.objects.get_or_create(name=rc_data.state.get('kernel_arch'))

            build = models.BuildRun.objects.get(
                pipeline=pipeline,
                kernel_arch=architecture,
            )
            build.kernel_package_url = rc_data.state.get('kernel_package_url')
            build.save()

        elif job_data.stage == 'test':
            # Add the tests to the pipeline.
            # We get one rc with many tests but only one architecture.

            architecture, _ = models.Architecture.objects.get_or_create(name=rc_data.state.get('kernel_arch'))

            logs_downloader = BeakerLogsDownloader(rc_data, pipeline)

            targeted_tests_list = []
            if rc_data.state.get('targeted_tests'):
                targeted_tests = gitlab_helper.get_artifact(job_data.id, rc_data.state.get('targeted_tests_list'),
                                                            raw=True)
                try:
                    targeted_tests_list = targeted_tests.decode().splitlines()
                except AttributeError:
                    pass

            for index, recipeset in enumerate(rc_data.state.get('recipesets', '').split()):

                recipeset_data = beaker.get_recipeset_tasks(recipeset)
                for recipe_id, recipe in recipeset_data.items():

                    host_id = index + 1

                    # Create recipe logs. We'll add them to the test_run later.
                    recipe_logs = []
                    for log_ in logs_downloader.download(recipe_id):
                        filename = f'{architecture.name}_{host_id}_{log_.name}'
                        log, created = models.Artifact.objects.get_or_create(
                            name=filename,
                            pipeline=pipeline,
                        )

                        if created or not log.exists():
                            log.file.save(filename, ContentFile(log_.sanitized_content))
                            log.save()
                        recipe_logs.append(log)

                    for test_index, test_info in enumerate(recipe):
                        print(f"Processing {test_index + 1} / {len(recipe)}")

                        # Get or create the test.
                        test, created = models.Test.objects.update_or_create(
                            name=test_info.get('cki_name'),
                            fetch_url=test_info.get('fetch_url'),
                            defaults={
                                'universal_id': test_info.get('universal_id')
                            }
                        )

                        # If just created the test, add the maintainers
                        if created:
                            for maintainer in test_info.get('maintainers'):
                                name, email = maintainer.replace(">", "").split(" <")
                                maintainer, _ = models.TestMaintainer.objects.get_or_create(
                                    name=name,
                                    email=email
                                )
                                test.maintainers.add(maintainer)

                        # Get the typified status and result
                        beaker_status, _ = models.BeakerStatus.objects.get_or_create(name=test_info.get('status'))
                        beaker_result, _ = models.BeakerResult.objects.get_or_create(name=test_info.get('result'))
                        beaker_resource, _ = models.BeakerResource.objects.get_or_create(
                            fqdn=test_info.get('beaker_resource_fqdn')
                        )

                        started_at = timestamp_to_datetime(test_info['start_time'])
                        finished_at = timestamp_to_datetime(test_info['finish_time'])

                        if started_at and finished_at:
                            duration = (finished_at - started_at).total_seconds()
                        else:
                            duration = None

                        kernel_debug = strtobool(rc_data.state.get('debug_kernel', 'no'))

                        # Create the BeakerTestRun instance
                        test_run, created = models.BeakerTestRun.objects.update_or_create(
                            job=job,
                            jid=job_data.id,
                            task_id=test_info.get('task_id').split(':')[1],  # T:123456
                            recipe_id=recipe_id,
                            pipeline=pipeline,
                            test=test,
                            defaults={
                                'beaker_result': beaker_result,
                                'beaker_status': beaker_status,
                                'waived': test_info.get('waived'),
                                'kernel_arch': architecture,
                                'kernel_debug': kernel_debug,
                                'beaker_resource': beaker_resource,
                                'retcode': rc_data.state.get('retcode'),
                                'started_at': started_at,
                                'finished_at': finished_at,
                                'duration': duration,
                                'targeted': test.name in targeted_tests_list,
                            }
                        )

                        # Get the test logs
                        test_logs = logs_downloader.download(recipe_id, test_run)
                        for log_ in test_logs:
                            filename = (f'{test.name_sanitized}/'
                                        f'{architecture.name}_{host_id}_{test.universal_id}_{log_.name}')
                            log, created = models.Artifact.objects.get_or_create(
                                name=filename,
                                pipeline=pipeline,
                            )
                            if created or not log.exists():
                                log.file.save(filename, ContentFile(log_.sanitized_content))
                                log.save()
                            test_run.logs.add(log)

                        [test_run.logs.add(log) for log in recipe_logs]  # pylint: disable=expression-not-assigned

                    update_invalid_results(recipe_id)


def check_pipeline(project_id, pipeline_id):
    """Check if a pipeline's data is healthy."""
    # pylint: disable=too-many-locals, too-many-nested-blocks, too-many-branches
    beaker = Beaker(settings.BEAKER_URL)
    gitlab_helper = gitlab.GitlabHelper(settings.GITLAB_URL, settings.GITLAB_TOKEN)
    gitlab_helper.set_project(project_id)

    for job_data, rc_data in get_rcs(project_id, pipeline_id):

        stage, _ = models.Stage.objects.get_or_create(name=job_data.stage)
        job, _ = models.Job.objects.get_or_create(name=job_data.name, stage=stage)

        project = models.Project.objects.get(
            project_id=project_id,
            path=gitlab_helper.project.attributes.get('path_with_namespace')
        )

        pipeline = models.Pipeline.objects.get(
            commit_id=job_data.commit.get('id'),
            pipeline_id=pipeline_id,
            project=project
        )

        try:
            patch_urls = pipeline.variables.get(key='patch_urls').value.split(' ')
        except models.TriggerVariable.DoesNotExist:
            pass
        else:
            if len(patch_urls) != pipeline.patches.count():
                raise ObjectDoesNotExist

        if job_data.stage == 'lint':
            lint = models.LintRun.objects.get(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
            )

            if lint.log and not lint.log.exists():
                raise ObjectDoesNotExist

        elif job_data.stage == 'merge':
            merge = models.MergeRun.objects.get(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
            )

            if merge.log and not merge.log.exists():
                raise ObjectDoesNotExist

        elif job_data.stage == 'build':
            build = models.BuildRun.objects.get(
                job=job,
                jid=job_data.id,
                pipeline=pipeline,
            )

            if build.log and not build.log.exists():
                raise ObjectDoesNotExist

        elif job_data.stage == 'publish':
            if project.path == 'cki-project/brew-pipeline':
                # rhel5 brew pipelines have a publish stage, but no build.
                # Ignore publish stages on brew pipelines.
                continue
            models.BuildRun.objects.get(
                pipeline=pipeline,
                kernel_package_url=rc_data.state.get('kernel_package_url'),
            )

        elif job_data.stage == 'test':
            for recipeset in rc_data.state.get('recipesets', '').split():
                recipeset_data = beaker.get_recipeset_tasks(recipeset)
                for recipe_id, recipe in recipeset_data.items():
                    for test_info in recipe:
                        test = models.Test.objects.get(
                            name=test_info.get('name'),
                            fetch_url=test_info.get('fetch_url')
                        )

                        started_at = timestamp_to_datetime(test_info['start_time'])
                        finished_at = timestamp_to_datetime(test_info['finish_time'])

                        test = models.BeakerTestRun.objects.get(
                            job=job,
                            jid=job_data.id,
                            task_id=test_info.get('task_id').split(':')[1],  # T:123456
                            recipe_id=recipe_id,
                            pipeline=pipeline,
                            test=test,
                            started_at=started_at,
                            finished_at=finished_at,
                        )

                        if not test.logs.exists():
                            raise ObjectDoesNotExist

                        for log in test.logs.all():
                            if not log.exists():
                                raise ObjectDoesNotExist


def bootstrap_projects():
    """Fill the projects table with all cki-project/*-pipeline projects."""
    bootstrapped = False

    if models.Project.objects.count() == 0:
        bootstrapped = True
        gitlab_helper = gitlab.GitlabHelper(settings.GITLAB_URL, settings.GITLAB_TOKEN)
        for project in gitlab_helper.api.projects.list():
            if (project.namespace['full_path'] == 'cki-project' and re.fullmatch(r'\w+-pipeline', project.path)):
                models.Project.objects.create(project_id=project.id, path=project.path_with_namespace)

    return {'bootstrapped': bootstrapped,
            'projects': [p.path for p in models.Project.objects.all()]}


def check_and_fix_pipelines(since_days=2, running=True):
    """
    Check all pipeline since {since_days} and fix them if not healthy.

    If running=True, check all pipelines without duration despite the date.
    """
    gitlab_helper = gitlab.GitlabHelper(settings.GITLAB_URL, settings.GITLAB_TOKEN)

    date_from = datetime.datetime.now() - datetime.timedelta(days=since_days)

    projects = models.Project.objects.all()

    for project in projects:
        gitlab_helper.set_project(project.project_id)
        pipeline_ids = [pipeline.id for pipeline in gitlab_helper.get_pipelines_since(date_from)]

        if running:
            pipeline_ids.extend(project.pipeline_set.filter(duration=None).values_list('pipeline_id', flat=True))

        for pipeline_id in pipeline_ids:
            try:
                check_pipeline(project.project_id, pipeline_id)
                continue
            except models.Pipeline.DoesNotExist:
                # The pipeline was never submitted
                print(f"Fixing missing pipeline: {pipeline_id}")
            except ObjectDoesNotExist:
                # The pipeline was submitted but it's not fully populated.
                # so we first want to delete it.
                print(f"Fixing broken pipeline: {pipeline_id}")
            except Exception as error:  # pylint: disable=broad-except
                capture_exception(error)
                continue

            try:
                submit_pipeline(project.project_id, pipeline_id)
            except Exception as error:  # pylint: disable=broad-except
                capture_exception(error)


def update_queued_tests():
    """Fix tests left in queued because the Gitlab job timeouted."""
    beaker = Beaker(settings.BEAKER_URL)
    incomplete_recipes = models.BeakerTestRun.objects.filter(beaker_result__name='New').distinct('recipe_id')

    for testrun in incomplete_recipes:
        recipe = beaker.get_recipe(testrun.recipe_id)
        for task in recipe['tasks']:
            try:
                testrun = models.BeakerTestRun.objects.get(task_id=task['id'])
            except models.BeakerTestRun.DoesNotExist:
                continue

            if task['result'] == 'New':
                # Still running
                continue

            if task['status'] == 'Aborted' and not recipe.get('resource'):
                logging.error("Task %s aborted and has no beaker_resource. Deleting.", task['id'])
                testrun.delete()
                continue

            beaker_status, _ = models.BeakerStatus.objects.get_or_create(name=task['status'])
            beaker_result, _ = models.BeakerResult.objects.get_or_create(name=task['result'])
            beaker_resource, _ = models.BeakerResource.objects.get_or_create(
                fqdn=recipe['resource']['fqdn']
            )

            testrun.beaker_status = beaker_status
            testrun.beaker_result = beaker_result
            testrun.beaker_resource = beaker_resource
            testrun.save()
