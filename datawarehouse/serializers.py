# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Serializers."""
from rest_framework import serializers
from . import models


# Misc
class ArchitectureSerializer(serializers.ModelSerializer):
    """Serializer for Architecture."""

    class Meta:
        """Metadata."""

        model = models.Architecture
        fields = ('name',)


class GitTreeSerializer(serializers.ModelSerializer):
    """Serializer for GitTree model."""

    class Meta:
        """Metadata."""

        model = models.GitTree
        fields = ('name',)


# Lint
class LintRunSerializer(serializers.ModelSerializer):
    """Serializer for LintRun model."""

    class Meta:
        """Metadata."""

        model = models.LintRun
        fields = ('jid', 'success', 'command')


# Merge
class PatchworkSubmitterSerializer(serializers.ModelSerializer):
    """Serializer for PatchworkSubmitter model."""

    class Meta:
        """Metadata."""

        model = models.PatchworkSubmitter
        fields = ('name', 'email', 'submitter_id')


class PatchworkProjectSerializer(serializers.ModelSerializer):
    """Serializer for PatchworkProject model."""

    class Meta:
        """Metadata."""

        model = models.PatchworkProject
        fields = ('name', 'project_id')


class SimplePatchSerializer(serializers.ModelSerializer):
    """Serializer for Patch model. Simple."""

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('url', 'subject')


class PatchworkSeriesSerializer(serializers.ModelSerializer):
    """Serializer for PatchworkSeries model."""

    submitter = PatchworkSubmitterSerializer()
    project = PatchworkProjectSerializer()

    class Meta:
        """Metadata."""

        model = models.PatchworkSeries
        fields = ('series_id', 'url', 'web_url', 'name', 'submitter', 'project', 'submitted_date', 'series_id')


class PatchworkPatchSerializer(serializers.ModelSerializer):
    """Serializer for PatchworkPatch model."""

    series = PatchworkSeriesSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.PatchworkPatch
        fields = ('url', 'web_url', 'subject', 'series', 'msgid', 'mbox', 'patch_id')


class PatchSerializer(serializers.ModelSerializer):
    """Serializer for Patch model. With inherited models."""

    def to_representation(self, instance):
        """Depending on the type, use a different serializer."""
        if hasattr(instance, 'patchworkpatch'):
            serializer = PatchworkPatchSerializer(instance.patchworkpatch)
        else:
            serializer = SimplePatchSerializer(instance)
        return serializer.data


class MergeRunSerializer(serializers.ModelSerializer):
    """Serializer for MergeRun model."""

    class Meta:
        """Metadata."""

        model = models.MergeRun
        fields = ('jid', 'success',)


# Build
class CompilerSerializer(serializers.ModelSerializer):
    """Serializer for Compiler model."""

    class Meta:
        """Metadata."""

        model = models.Compiler
        fields = ('name',)


class BuildRunSerializer(serializers.ModelSerializer):
    """Serializer for BuildRun model."""

    kernel_arch = ArchitectureSerializer()
    compiler = CompilerSerializer()

    class Meta:
        """Metadata."""

        model = models.BuildRun
        fields = ('jid', 'success',
                  'kernel_arch', 'make_opts', 'url', 'repo_path',
                  'kernel_package_url', 'duration', 'compiler')


# Test
class TestMaintainerSerializer(serializers.ModelSerializer):
    """Serializer for TestMaintainer model."""

    class Meta:
        """Metadata."""

        model = models.TestMaintainer
        fields = ('name', 'email')


class TestSerializer(serializers.ModelSerializer):
    """Serializer for Test model."""

    maintainers = TestMaintainerSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Test
        fields = ('id', 'name', 'fetch_url', 'maintainers', 'universal_id',)


class TestSimpleSerializer(TestSerializer):
    """Serializer for Test model. Simple."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = ('id', 'name', 'fetch_url')


class TestConfidenceSerializer(TestSerializer):
    """Serializer for Test model. With confidence metrics."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = TestSerializer.Meta.fields + ('confidence',)


class BeakerResultSerializer(serializers.ModelSerializer):
    """Serializer for BeakerResult model."""

    class Meta:
        """Metadata."""

        model = models.BeakerResult
        fields = ('name',)


class BeakerStatusSerializer(serializers.ModelSerializer):
    """Serializer for BeakerStatus model."""

    class Meta:
        """Metadata."""

        model = models.BeakerStatus
        fields = ('name',)


class ArtifactSerializer(serializers.ModelSerializer):
    """Serializer for Artifact model."""

    # Force file property to be handled as FileField.
    file = serializers.FileField()

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('name', 'file',)


class TestResultSerializer(serializers.ModelSerializer):
    """Serializer for TestResult model."""

    class Meta:
        """Metadata."""

        model = models.TestResult
        fields = ('name',)


class BeakerResourceSerializer(serializers.ModelSerializer):
    """Serializer for BeakerResource model."""

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('id', 'fqdn',)


class BeakerResourceConfidenceSerializer(BeakerResourceSerializer):
    """Serializer for BeakerResource model. With confidence metrics."""

    class Meta(BeakerResourceSerializer.Meta):
        """Metadata."""

        fields = BeakerResourceSerializer.Meta.fields + ('confidence',)


class SimpleTestRunSerializer(serializers.ModelSerializer):
    """Serializer for TestRun model."""

    test = TestSerializer()
    kernel_arch = ArchitectureSerializer()
    logs = ArtifactSerializer(many=True)
    trace_url = serializers.SerializerMethodField('get_trace_url')
    result = TestResultSerializer()

    class Meta:
        """Metadata."""

        model = models.TestRun
        fields = ('id', 'test', 'waived', 'kernel_arch', 'kernel_debug',
                  'logs', 'invalid_result', 'untrusted', 'targeted', 'trace_url',
                  'result',
                  )

    def get_trace_url(self, testrun):
        # pylint: disable=no-self-use
        """Return trace_url if available."""
        try:
            return testrun.beakertestrun.trace_url
        except AttributeError:
            return None


class BeakerTestRunSerializer(SimpleTestRunSerializer):
    """Serializer for BeakerTestRun model."""

    beaker_result = BeakerResultSerializer()
    beaker_status = BeakerStatusSerializer()
    beaker_resource = BeakerResourceSerializer()

    class Meta(SimpleTestRunSerializer.Meta):
        """Metadata."""

        model = models.BeakerTestRun
        fields = SimpleTestRunSerializer.Meta.fields + \
            ('beaker_result', 'beaker_status', 'beaker_resource',
             'retcode', 'task_id', 'recipe_id', 'recipe_url', 'duration')


class TestRunSerializer(serializers.ModelSerializer):
    """Serializer for TestRun model. With inherited models."""

    def to_representation(self, instance):
        """Depending on the type, use a different serializer."""
        if hasattr(instance, 'beakertestrun'):
            serializer = BeakerTestRunSerializer(instance.beakertestrun)
        else:
            serializer = SimpleTestRunSerializer(instance)
        return serializer.data


class ProjectSerializer(serializers.ModelSerializer):
    """Serializer for Project model."""

    class Meta:
        """Metadata."""

        model = models.Project
        fields = ('project_id', 'path')


class RecipientSerializer(serializers.ModelSerializer):
    """Serializer for Recipient model."""

    class Meta:
        """Metadata."""

        model = models.Recipient
        fields = ("email",)


class ReportSerializer(serializers.ModelSerializer):
    """Serializer for Report model."""

    addr_to = RecipientSerializer(many=True)
    addr_cc = RecipientSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Report
        fields = ("__all__")


# Pipeline
class PipelineSerializer(serializers.ModelSerializer):
    """Serializer for Pipeline model."""

    lint_jobs = LintRunSerializer(many=True)
    merge_jobs = MergeRunSerializer(many=True)
    build_jobs = BuildRunSerializer(many=True)
    test_jobs = TestRunSerializer(many=True)
    gittree = GitTreeSerializer()
    project = ProjectSerializer()
    patches = PatchSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Pipeline
        fields = ('commit_id', 'commit_message_title', 'project', 'gittree', 'web_url', 'pipeline_id',
                  'created_at', 'started_at', 'finished_at', 'duration',
                  'lint_jobs', 'merge_jobs', 'build_jobs', 'test_jobs',
                  'kernel_type', 'test_hash', 'tag', 'make_target', 'kernel_version',
                  'test_passed', 'build_passed', 'merge_passed', 'lint_passed',
                  'test_passed_status', 'build_passed_status', 'merge_passed_status', 'lint_passed_status',
                  'all_passed', 'all_passed_status', 'trigger_variables', 'patches',
                  )


class PipelineSimpleSerializer(PipelineSerializer):
    """Serializer for Pipeline model. Simple."""

    class Meta(PipelineSerializer.Meta):
        """Metadata."""

        fields = ('commit_id', 'project', 'web_url', 'pipeline_id',
                  'created_at', 'started_at', 'finished_at', 'duration',
                  'test_passed', 'build_passed', 'merge_passed', 'lint_passed',
                  'all_passed', 'gittree', 'kernel_version',
                  )


class PipelineOrderedSerializer(PipelineSerializer):
    """Serializer for Pipeline model. With some things tweaked."""

    test_jobs = serializers.SerializerMethodField('nested_test_jobs')

    def nested_test_jobs(self, pipeline):
        """
        Render the test jobs in a friendly way for the reporter.

        [{
          arch: Architecture,
          hosts: [
            {host: BeakerResource, tests: [TestRun,]},
          ]
        }]

        Should be improved.
        """
        # Get the architectures where this pipe's tests run.
        archs = models.Architecture.objects.filter(testrun__pipeline=pipeline).distinct()
        nested = []

        for arch in archs:
            hosts = []
            # Get the beaker hosts where this pipe's tests run on architecture {arch}
            beaker_machines = models.BeakerResource.objects.filter(beakertestrun__pipeline=pipeline,
                                                                   beakertestrun__kernel_arch=arch).distinct()

            # Put the machine together with the tests it run.
            for beaker_machine in beaker_machines:
                tests = pipeline.test_jobs.filter(beakertestrun__beaker_resource=beaker_machine,
                                                  kernel_arch=arch).exclude(beakertestrun__beaker_result__name='Skip')\
                                                                   .order_by('waived', 'test__name')
                hosts.append({
                    'host': BeakerResourceSerializer(beaker_machine).data,
                    'tests': TestRunSerializer(tests, many=True, context=self.context).data
                })

            nested.append({'arch': ArchitectureSerializer(arch).data, 'hosts': hosts})

        return nested


class TriagePipelineSerializer(serializers.ModelSerializer):
    """Serializer for triaging Pipeline model."""

    lint_jobs = serializers.SerializerMethodField('failed_lint')
    merge_jobs = serializers.SerializerMethodField('failed_merge')
    build_jobs = serializers.SerializerMethodField('failed_build')
    test_jobs = serializers.SerializerMethodField('failed_test')

    class Meta:
        """Metadata."""

        model = models.Pipeline
        fields = ('pipeline_id', 'lint_jobs', 'merge_jobs', 'build_jobs', 'test_jobs',)

    def failed_lint(self, pipeline):
        # pylint: disable=no-self-use
        """Serialize failed lint_jobs."""
        return LintRunSerializer(
            pipeline.lint_jobs.filter(success=False),
            many=True).data

    def failed_merge(self, pipeline):
        # pylint: disable=no-self-use
        """Serialize failed merge_jobs."""
        return MergeRunSerializer(
            pipeline.merge_jobs.filter(success=False),
            many=True).data

    def failed_build(self, pipeline):
        # pylint: disable=no-self-use
        """Serialize failed build_jobs."""
        return BuildRunSerializer(
            pipeline.build_jobs.filter(success=False),
            many=True).data

    def failed_test(self, pipeline):
        # pylint: disable=no-self-use
        """Serialize failed test_jobs."""
        return TestRunSerializer(
            pipeline.test_jobs.filter(passed=False, untrusted=False)
            .select_related('test', 'beakertestrun', 'kernel_arch', 'pipeline__project')
            .prefetch_related('test__maintainers', 'logs'),
            many=True).data


class IssueKindSerializer(serializers.ModelSerializer):
    """Serializer for IssueKind model."""

    class Meta:
        """Metadata."""

        model = models.IssueKind
        fields = ('id', 'description', 'tag',)


class IssueSerializer(serializers.ModelSerializer):
    """Serializer for Issue model."""

    kind = IssueKindSerializer()

    class Meta:
        """Metadata."""

        model = models.Issue
        fields = ('id', 'kind', 'description', 'ticket_url', 'resolved', 'generic')


class IssueRecordSerializer(serializers.ModelSerializer):
    """Serializer for IssueRecord model."""

    issue = IssueSerializer()
    pipeline = PipelineSimpleSerializer()
    testruns = TestRunSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.IssueRecord
        fields = ('id', 'issue', 'pipeline', 'testruns', 'bot_generated')
