import json

from django.template import Library

from datawarehouse import serializers


register = Library()


@register.filter
def serialize(obj, serializer_name):
    serializer = getattr(serializers, serializer_name)
    data = serializer(obj).data
    return json.dumps(data)
