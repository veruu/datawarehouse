# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the files."""

import os
from django.conf import settings
from django.db import models
from django_s3_storage.storage import S3Storage

from . import pipeline_models


S3_BUCKET = S3Storage(aws_s3_bucket_name=settings.AWS_S3_BUCKET_NAME)


def s3_directory_path(instance, filename):
    """Return path for Artifact in AWS S3."""
    # For some reason, instance.pipeline.created_at is a string instead of a datetime.
    # This hack is to workaround that. Should be fixed :)
    pipeline = pipeline_models.Pipeline.objects.get(pipeline_id=instance.pipeline.pipeline_id)
    formatted_date = pipeline.created_at.strftime("%Y/%m/%d")
    return f"{formatted_date}/{instance.pipeline.pipeline_id}/{filename}"


class Artifact(models.Model):
    """Model for Artifact."""

    file_local = models.FileField(upload_to=pipeline_models.pipeline_directory_path, null=True)
    file_remote = models.FileField(upload_to=s3_directory_path,
                                   storage=S3_BUCKET, null=True)
    pipeline = models.ForeignKey(pipeline_models.Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='artifacts')
    name = models.CharField(max_length=150)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    @property
    def file(self):
        """
        Dynamically decide where to store the file.

        If not on production, always locally. Otherwise, use gittree to decide.
        """
        if not settings.IS_PRODUCTION:
            return self.file_local
        if self.pipeline.gittree.internal:
            return self.file_local

        # Keep compatibility for artifacts already stored locally.
        if self.file_local:
            return self.file_local

        return self.file_remote

    @property
    def tail(self):
        """Get last 50 lines of the file."""
        try:
            return "".join(line.decode() for line in self.file.readlines()[-50:])
        except FileNotFoundError:
            return "Error: Log file not found"

    def exists(self):
        """Check if the file exists."""
        if self.file_local:
            return os.path.isfile(self.file_local.path)
        if self.file_remote:
            return S3_BUCKET.exists(self.file_remote.name)
        return False
