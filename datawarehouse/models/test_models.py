# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the test stage."""

from django.conf import settings
from django.db import models

from datawarehouse import styles
from . import pipeline_models


class TestMaintainer(models.Model):
    """Model for TestMaintainer."""

    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'


class TestManager(models.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, name, fetch_url):
        """Lookup the object by the natural key."""
        return self.get(name=name, fetch_url=fetch_url)


class Test(models.Model):
    """Model for Test."""

    name = models.CharField(max_length=200)
    fetch_url = models.URLField(blank=True, null=True)
    maintainers = models.ManyToManyField(TestMaintainer)
    universal_id = models.CharField(max_length=50, null=True)

    objects = TestManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, self.fetch_url)

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.testrun_set
                  .filter(invalid_result=False)
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1

    @property
    def name_sanitized(self):
        """Turn a string into a path-compatible name."""
        return "".join(
            char if char.isalnum() else '_' for char in self.name
        )


class BeakerResult(models.Model):
    """Model for BeakerResult."""

    name = models.CharField(max_length=10)

    objects = pipeline_models.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class BeakerStatus(models.Model):
    """Model for BeakerStatus."""

    name = models.CharField(max_length=10)

    objects = pipeline_models.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class BeakerResourceManager(models.Manager):
    """Natural key for BeakerResource."""

    def get_by_natural_key(self, fqdn):
        """Lookup the object by the natural key."""
        return self.get(fqdn=fqdn)


class BeakerResource(models.Model):
    """Model for BeakerResource."""

    fqdn = models.CharField(max_length=100, blank=True, null=True)

    objects = BeakerResourceManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.fqdn}'

    def natural_key(self):
        """Return the natural key."""
        return (self.fqdn, )

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.beakertestrun_set
                  .filter(invalid_result=False)
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1


class TestResult(models.Model):
    """Model for TestResult."""

    name = models.CharField(max_length=10)

    objects = pipeline_models.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class TestRun(models.Model):
    """Model for TestRun."""

    pipeline = models.ForeignKey(pipeline_models.Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='test_jobs')
    test = models.ForeignKey(Test, on_delete=models.PROTECT)
    waived = models.BooleanField()
    kernel_arch = models.ForeignKey(pipeline_models.Architecture, on_delete=models.PROTECT)
    kernel_debug = models.BooleanField(default=False)
    logs = models.ManyToManyField('Artifact')
    invalid_result = models.BooleanField(default=False)
    untrusted = models.BooleanField(default=False)
    passed = models.BooleanField(default=False)
    result = models.ForeignKey(TestResult, on_delete=models.PROTECT)
    targeted = models.BooleanField(default=False)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.pipeline.pipeline_id}: {self.kernel_arch.name} - {self.test.name}'

    def save(self, *args, **kwargs):
        # pylint: disable=arguments-differ
        """
        Override save method.

        Calculate if we trust on the test result value and
        save it on the untrusted var.
        """
        self.untrusted = self.invalid_result or self.waived
        super().save(*args, **kwargs)

    @property
    def passed_style(self):
        """Style according to the passed value.

        Is modified if the result was waived or invalid.
        """
        return styles.get_style(self.result.name, untrusted=self.untrusted)


class BeakerTestRun(TestRun):
    """Model for BeakerTestRun."""

    job = models.ForeignKey(pipeline_models.Job, on_delete=models.PROTECT)
    jid = models.IntegerField()  # Gitlab's Job ID
    task_id = models.IntegerField()  # Beaker Task ID
    recipe_id = models.IntegerField()  # Beaker Recipe ID
    beaker_result = models.ForeignKey(BeakerResult, on_delete=models.PROTECT)
    beaker_status = models.ForeignKey(BeakerStatus, on_delete=models.PROTECT)
    beaker_resource = models.ForeignKey(BeakerResource, on_delete=models.PROTECT)
    started_at = models.DateTimeField(null=True)
    finished_at = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)
    retcode = models.IntegerField()

    def save(self, *args, **kwargs):
        # pylint: disable=arguments-differ
        """Override save method."""
        self.result, _ = TestResult.objects.get_or_create(name=self.passed_result)
        self.passed = self.result.name not in ('ERROR', 'FAIL')
        super().save(*args, **kwargs)

    @property
    def recipe_url(self):
        """Return formatted recipe URL."""
        return f'{settings.BEAKER_URL}/recipes/{self.recipe_id}'

    @property
    def trace_url(self):
        """Return URL to trace file."""
        return (
            f'{settings.GITLAB_URL}/api/v4/projects/{self.pipeline.project.project_id}'
            f'/jobs/{self.jid}/trace'
        )

    @property
    def passed_result(self):
        """Beaker-independent result.

        - SKIP:  Not yet cancelled/aborted/completed, maybe not at all going to run; skipped.
        - PASS:  Everything ok, nothing to see here.
        - FAIL:  Look at me, I'm a failed test.
        - ERROR: Not an expected test issue.
        """
        if self.beaker_status.name not in ('Completed', 'Aborted', 'Cancelled'):
            return 'SKIP'
        if self.beaker_result.name == 'Skip':
            return 'SKIP'
        if self.beaker_result.name == 'Pass':
            return 'PASS'
        if self.beaker_result.name == 'Fail':
            return 'FAIL'
        return 'ERROR'
