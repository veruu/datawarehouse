# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods,too-many-public-methods

"""Pipeline models file."""

from django.conf import settings
from django.db import models

from datawarehouse import styles
from datawarehouse.models import test_models, issue_models, report_models


def pipeline_directory_path(instance, filename):
    """Return pipeline_id from instance."""
    if not hasattr(instance, 'pipeline'):
        return ""
    return f"{instance.pipeline.pipeline_id}/{filename}"


class GenericNameManager(models.Manager):
    """Natural key based on name."""

    def get_by_natural_key(self, name):
        """Lookup the object by the natural key."""
        return self.get(name=name)


class ProjectManager(models.Manager):
    """Natural key for Project."""

    def get_by_natural_key(self, path):
        """Lookup the object by the natural key."""
        return self.get(path=path)


class Project(models.Model):
    """Model for Project."""

    project_id = models.IntegerField()
    path = models.CharField(max_length=100)

    objects = ProjectManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.path}'

    def natural_key(self):
        """Return the natural key."""
        return (self.path, )


class GitTree(models.Model):
    """Model for GitTree."""

    name = models.CharField(max_length=20)
    internal = models.BooleanField(default=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'


class PipelineQuerySet(models.QuerySet):
    """Natural key for Pipeline."""

    def get_by_natural_key(self, pipeline_id):
        """Lookup the object by the natural key."""
        return self.get(pipeline_id=pipeline_id)

    def add_stats(self):
        """Add aggregate information about the various jobs."""
        stats_lint_fail_count = (LintRun.objects
                                 .filter(pipeline_id=models.OuterRef('id'),
                                         success=False)
                                 .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_lint_fail_count.query.set_group_by()
        stats_merge_fail_count = (MergeRun.objects
                                  .filter(pipeline_id=models.OuterRef('id'),
                                          success=False)
                                  .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_merge_fail_count.query.set_group_by()
        stats_build_fail_count = (BuildRun.objects
                                  .filter(pipeline_id=models.OuterRef('id'),
                                          success=False)
                                  .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_build_fail_count.query.set_group_by()
        stats_test_error_count = (test_models.TestRun.objects
                                  .filter(pipeline_id=models.OuterRef('id'), result__name='ERROR',
                                          untrusted=False)
                                  .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_test_error_count.query.set_group_by()
        stats_test_fail_count = (test_models.TestRun.objects
                                 .filter(pipeline_id=models.OuterRef('id'), result__name='FAIL',
                                         untrusted=False)
                                 .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_test_fail_count.query.set_group_by()
        stats_test_pass_count = (test_models.TestRun.objects
                                 .filter(pipeline_id=models.OuterRef('id'), result__name='PASS',
                                         untrusted=False)
                                 .values('pipeline_id').annotate(c=models.Count('*')).values('c'))
        stats_test_pass_count.query.set_group_by()
        return self.annotate(
            stats_lint_exists=models.Exists(LintRun.objects.filter(pipeline_id=models.OuterRef('id'))),
            stats_merge_exists=models.Exists(MergeRun.objects.filter(pipeline_id=models.OuterRef('id'))),
            stats_build_exists=models.Exists(BuildRun.objects.filter(pipeline_id=models.OuterRef('id'))),
            stats_test_exists=models.Exists(test_models.TestRun.objects.filter(pipeline_id=models.OuterRef('id'))),
            stats_lint_fail_count=models.Subquery(stats_lint_fail_count, output_field=models.IntegerField()),
            stats_merge_fail_count=models.Subquery(stats_merge_fail_count, output_field=models.IntegerField()),
            stats_build_fail_count=models.Subquery(stats_build_fail_count, output_field=models.IntegerField()),
            stats_test_error_count=models.Subquery(stats_test_error_count, output_field=models.IntegerField()),
            stats_test_pass_count=models.Subquery(stats_test_pass_count, output_field=models.IntegerField()),
            stats_test_fail_count=models.Subquery(stats_test_fail_count, output_field=models.IntegerField()),
            stats_test_targeted_exists=models.Exists(
                test_models.TestRun.objects.filter(pipeline_id=models.OuterRef('id'), targeted=True)
            ),
            stats_has_issue_records=models.Exists(
                issue_models.IssueRecord.objects.filter(pipeline_id=models.OuterRef('id'))
            ),
            stats_has_issue_records_bot_generated=models.Exists(
                issue_models.IssueRecord.objects.filter(pipeline_id=models.OuterRef('id'), bot_generated=True)
            ),
            stats_report_exists=models.Exists(report_models.Report.objects.filter(pipeline_id=models.OuterRef('id'))),
        ).prefetch_related('issue_records').select_related('gittree')


class Pipeline(models.Model):
    """Model for Pipeline."""

    commit_id = models.CharField(max_length=40)
    commit_message_title = models.CharField(max_length=300, blank=True, null=True)
    pipeline_id = models.IntegerField(unique=True)
    project = models.ForeignKey(Project, on_delete=models.PROTECT)
    gittree = models.ForeignKey(GitTree, on_delete=models.PROTECT, null=True)
    kernel_type = models.CharField(max_length=20, blank=True, null=True)
    test_hash = models.CharField(max_length=40, blank=True, null=True)
    tag = models.CharField(max_length=40, blank=True, null=True)
    make_target = models.CharField(max_length=40, blank=True, null=True)
    kernel_version = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(null=True)
    started_at = models.DateTimeField(null=True)
    finished_at = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)

    objects = PipelineQuerySet().as_manager()

    class Meta:
        """Metadata."""

        ordering = ('-pipeline_id',)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.commit_id}'

    def natural_key(self):
        """Return the natural key."""
        return (self.pipeline_id, )

    @property
    def trigger_variables(self):
        """Flat dict of variables."""
        return {var.key: var.value for var in self.variables.all()}

    @property
    def web_url(self):
        """Return formatted url of pipeline."""
        return f'{settings.GITLAB_URL}/{self.project.path}' + \
            f'/pipelines/{self.pipeline_id}'

    @property
    def lint_exists(self):
        """Any lint job available?."""
        try:
            return self.stats_lint_exists  # pylint: disable=no-member
        except AttributeError:
            # allow prefetching/caching of the jobs
            return len(list(self.lint_jobs.all())) > 0

    @property
    def lint_passed(self):
        """Lint passed?."""
        try:
            return not self.stats_lint_fail_count  # pylint: disable=no-member
        except AttributeError:
            # filtering in Python on a list to allow prefetching/caching of the jobs
            return all(j.success for j in list(self.lint_jobs.all()))

    @property
    def lint_passed_status(self):
        """Lint passed status."""
        if not self.duration:
            return "Running"
        if self.lint_exists:
            return "PASS" if self.lint_passed else "FAIL"

        return "SKIP"

    @property
    def lint_stage(self):
        """Lint stage info."""
        return {
            'passed': self.lint_passed,
            'status': self.lint_passed_status,
            'style': styles.get_style(self.lint_passed_status),
            'name': 'Lint',
        }

    @property
    def has_targeted_tests(self):
        """Return if the Pipeline has TestRuns targeted."""
        try:
            return self.stats_test_targeted_exists
        except AttributeError:
            return self.test_jobs.filter(targeted=True).exists()

    @property
    def valid_tests(self):
        """Filter out waived and invalid test results."""
        # filtering in Python on a list to allow prefetching/caching of the jobs
        return [t for t in list(self.test_jobs.all()) if not (t.waived or t.invalid_result)]

    @property
    def test_passed(self):
        """Return whether all tests passed.

        Checks that all of the valid test jobs are PASS.
        """
        try:
            return (not self.stats_test_fail_count) and (not self.stats_test_error_count)  # pylint: disable=no-member
        except AttributeError:
            return all(t.passed for t in self.valid_tests)

    @property
    def test_passed_status(self):
        """Test passed status.

        Returns 'Running' or the worst result of the related test jobs.
        """
        if not self.duration:
            return "Running"
        try:
            if self.stats_test_error_count:  # pylint: disable=no-member
                return 'ERROR'
            if self.stats_test_fail_count:  # pylint: disable=no-member
                return 'FAIL'
            if self.stats_test_pass_count:  # pylint: disable=no-member
                return 'PASS'
        except AttributeError:
            results = [t.result.name for t in self.valid_tests]
            for value in ('ERROR', 'FAIL', 'PASS'):
                if value in results:
                    return value
        return 'SKIP'

    @property
    def test_stage(self):
        """Test stage info."""
        return {
            'passed': self.test_passed,
            'status': self.test_passed_status,
            'style': styles.get_style(self.test_passed_status),
            'name': 'Test',
        }

    @property
    def build_exists(self):
        """Any build job available?."""
        try:
            return self.stats_build_exists  # pylint: disable=no-member
        except AttributeError:
            # allow prefetching/caching of the jobs
            return len(list(self.build_jobs.all())) > 0

    @property
    def build_passed(self):
        """Build passed?."""
        try:
            return not self.stats_build_fail_count  # pylint: disable=no-member
        except AttributeError:
            # filtering in Python on a list to allow prefetching/caching of the jobs
            return all(j.success for j in list(self.build_jobs.all()))

    @property
    def build_passed_status(self):
        """Build passed status."""
        if not self.duration:
            return "Running"
        if self.build_exists:
            return "PASS" if self.build_passed else "FAIL"

        return "SKIP"

    @property
    def build_stage(self):
        """Build stage info."""
        return {
            'passed': self.build_passed,
            'status': self.build_passed_status,
            'style': styles.get_style(self.build_passed_status),
            'name': 'Build',
        }

    @property
    def merge_exists(self):
        """Any merge job available?."""
        try:
            return self.stats_merge_exists  # pylint: disable=no-member
        except AttributeError:
            # allow prefetching/caching of the jobs
            return len(list(self.merge_jobs.all())) > 0

    @property
    def merge_passed(self):
        """Merge passed?."""
        try:
            return not self.stats_merge_fail_count  # pylint: disable=no-member
        except AttributeError:
            # filtering in Python on a list to allow prefetching/caching of the jobs
            return all(j.success for j in list(self.merge_jobs.all()))

    @property
    def merge_passed_status(self):
        """Merge passed status."""
        if not self.duration:
            return "Running"
        if self.merge_exists:
            return "PASS" if self.merge_passed else "FAIL"

        return "SKIP"

    @property
    def merge_stage(self):
        """Merge stage info."""
        return {
            'passed': self.merge_passed,
            'status': self.merge_passed_status,
            'style': styles.get_style(self.merge_passed_status),
            'name': 'Merge',
        }

    @property
    def all_passed(self):
        """All passed?."""
        return (
            self.lint_passed and
            self.merge_passed and
            self.build_passed and
            self.test_passed
        )

    @property
    def all_passed_status(self):
        """All passed status."""
        if not self.duration:
            return "Running"
        return "PASS" if self.all_passed else "FAIL"

    @property
    def all_stage(self):
        """All stages info."""
        return {
            'passed': self.all_passed,
            'status': self.all_passed_status,
            'style': styles.get_style(self.all_passed_status),
            'name': 'General',
        }

    @property
    def report_exists(self):
        """Any report available?."""
        try:
            return self.stats_report_exists  # pylint: disable=no-member
        except AttributeError:
            # allow prefetching/caching of the jobs
            return len(list(self.reports.all())) > 0


class TriggerVariable(models.Model):
    """Model for TriggerVariable."""

    key = models.CharField(max_length=100)
    value = models.TextField()
    pipeline = models.ForeignKey(Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='variables')

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.key}: {self.value}'


class Stage(models.Model):
    """Model for Stage."""

    name = models.CharField(max_length=100)

    objects = GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class JobManager(models.Manager):
    """Natural key for Job."""

    def get_by_natural_key(self, name, stage):
        """Lookup the object by the natural key."""
        return self.get(name=name, stage__name=stage)


class Job(models.Model):
    """Model for Job."""

    name = models.CharField(max_length=100)
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE)

    objects = JobManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, ) + self.stage.natural_key()

    natural_key.dependencies = ['datawarehouse.stage']


class Architecture(models.Model):
    """Model for Architecture."""

    name = models.CharField(max_length=10)

    objects = GenericNameManager()

    class Meta:
        """Metadata."""

        ordering = ('name',)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, )


class Compiler(models.Model):
    """Model for Compiler."""

    name = models.CharField(max_length=120)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'


# Lint stage related
class LintRun(models.Model):
    """Model for LintRun."""

    job = models.ForeignKey(Job, on_delete=models.PROTECT)
    jid = models.IntegerField(unique=True)  # Gitlab's Job ID
    command = models.TextField()
    pipeline = models.ForeignKey(Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='lint_jobs')
    success = models.BooleanField()
    log = models.ForeignKey('Artifact', on_delete=models.PROTECT, null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.jid}'

    @property
    def success_style(self):
        """Style according to the success value."""
        return styles.get_style('PASS' if self.success else 'FAIL')

    def delete(self, *args, **kwargs):
        # pylint: disable=arguments-differ, no-member
        """Delete FKs."""
        super().delete(*args, **kwargs)
        self.log.delete()


class MergeRun(models.Model):
    """Model for MergeRun."""

    job = models.ForeignKey(Job, on_delete=models.PROTECT)
    jid = models.IntegerField(unique=True)  # Gitlab's Job ID
    pipeline = models.ForeignKey(Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='merge_jobs')
    success = models.BooleanField()
    log = models.ForeignKey('Artifact', on_delete=models.PROTECT, null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.jid}'

    @property
    def success_style(self):
        """Style according to the success value."""
        return styles.get_style('PASS' if self.success else 'FAIL')

    def delete(self, *args, **kwargs):
        # pylint: disable=arguments-differ, no-member
        """Delete FKs."""
        super().delete(*args, **kwargs)
        self.log.delete()


# Build stage related
class BuildRun(models.Model):
    """Model for BuildRun."""

    job = models.ForeignKey(Job, on_delete=models.PROTECT)
    jid = models.IntegerField(unique=True)  # Gitlab's Job ID
    pipeline = models.ForeignKey(Pipeline,
                                 on_delete=models.CASCADE,
                                 related_name='build_jobs')
    success = models.BooleanField()
    kernel_arch = models.ForeignKey(Architecture, on_delete=models.PROTECT)
    make_opts = models.TextField(blank=True, null=True)
    kernel_package_url = models.URLField(blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    repo_path = models.CharField(max_length=50, blank=True, null=True)
    log = models.ForeignKey('Artifact', on_delete=models.PROTECT, null=True,
                            related_name='build_log')
    artifacts = models.ManyToManyField('Artifact', related_name='build_artifacts')
    duration = models.IntegerField(null=True)
    compiler = models.ForeignKey(Compiler, null=True, on_delete=models.PROTECT)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.jid}'

    @property
    def success_style(self):
        """Style according to the success value."""
        return styles.get_style('PASS' if self.success else 'FAIL')

    def delete(self, *args, **kwargs):
        # pylint: disable=arguments-differ, no-member
        """Delete FKs."""
        super().delete(*args, **kwargs)
        self.log.delete()
