# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Utils file."""
import datetime
import email

from requests_futures.sessions import FuturesSession

from django.conf import settings
from django.utils.timezone import make_aware


def parse_patches_from_urls(patch_urls):
    """
    Get patch subject from list of patches' urls.

    The result respects the order of the input.
    """
    session = FuturesSession(max_workers=settings.REQUESTS_MAX_WORKERS)
    return [{
        'url': p,
        'subject': email.message_from_bytes(s.result().content)['Subject']
    } for p, s in [(p, session.get(p)) for p in patch_urls]]


def timestamp_to_datetime(timestamp):
    """Convert a timestamp string to a _timezone-aware_ datetime, while keeping None values."""
    try:
        return make_aware(
            datetime.datetime.fromisoformat(timestamp)
        )
    except TypeError:
        return None
