"""Urls file."""
from django.urls import path

from . import views

urlpatterns = [
    path('data/<str:kind>', views.KCIDBView.as_view(),),
]
