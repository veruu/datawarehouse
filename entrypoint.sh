#! /bin/bash
python manage.py migrate
python manage.py collectstatic
if [ -z "${IS_PRODUCTION:-}" ]; then
    python manage.py runserver 0.0.0.0:8000
else
    supervisord
    tail --follow=name --retry /var/log/nginx/dw-{access,error}.log
fi
