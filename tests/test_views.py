"""Test the views module."""
import datetime
import tempfile
import shutil
from django.utils import timezone
from django.core.files.base import ContentFile

from tests import utils
from tests.test_patches import mock_series

from datawarehouse import models, patches


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_trees = [
            models.GitTree.objects.create(name='Tree 1'),
            models.GitTree.objects.create(name='Tree 2'),
        ]
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project,
                                           gittree=self.git_trees[0]),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project,
                                           gittree=self.git_trees[1]),
        ]
        self.hosts = [
            models.BeakerResource.objects.create(id=1, fqdn='host_1'),
            models.BeakerResource.objects.create(id=2, fqdn='host_2'),
        ]
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.tests = [
            models.Test.objects.create(id=1, name='test_1'),
            models.Test.objects.create(id=2, name='test_2'),
        ]
        self.beaker_statuses = [
            models.BeakerStatus.objects.create(name='Completed'),
            models.BeakerStatus.objects.create(name='Aborted'),
        ]
        self.beaker_results = [
            models.BeakerResult.objects.create(name='Fail'),
            models.BeakerResult.objects.create(name='Pass'),
        ]
        self.stage = models.Stage.objects.create(name='stage')
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stage),
            models.Job.objects.create(name='job_2', stage=self.stage),
            models.Job.objects.create(name='job_3', stage=self.stage),
            models.Job.objects.create(name='job_4', stage=self.stage),
            ]

        # pylint: disable=line-too-long
        tests_list = [
            (0, self.jobs[0], self.pipelines[0], self.tests[0], self.beaker_results[1], self.beaker_statuses[0], self.hosts[1]), # noqa
            (1, self.jobs[1], self.pipelines[0], self.tests[1], self.beaker_results[0], self.beaker_statuses[1], self.hosts[0]), # noqa
            (2, self.jobs[2], self.pipelines[1], self.tests[0], self.beaker_results[1], self.beaker_statuses[1], self.hosts[0]), # noqa
            (3, self.jobs[3], self.pipelines[1], self.tests[1], self.beaker_results[0], self.beaker_statuses[0], self.hosts[1]), # noqa
        ]

        self.tmpdir = tempfile.TemporaryDirectory().name
        self.test_runs = []
        for incremental_id, job, pipeline, test_, beaker_result, beaker_status, host in tests_list:
            test_run = models.BeakerTestRun.objects.create(
                job=job, jid=incremental_id, task_id=incremental_id, recipe_id=incremental_id,
                pipeline=pipeline, test=test_, beaker_result=beaker_result, beaker_status=beaker_status,
                waived=False, kernel_arch=self.kernel_arch, beaker_resource=host, retcode=0
            )
            with self.settings(MEDIA_ROOT=self.tmpdir):
                log = models.Artifact.objects.create(
                    name=f'{test_.name}.log',
                    pipeline=pipeline,
                )
                log.file.save(f'{test_.name}_dummy.log', ContentFile(b'dummy log'))
                log.save()
                test_run.logs.add(log)
            self.test_runs.append(test_run)

    def tearDown(self):
        """TearDown."""
        shutil.rmtree(self.tmpdir)

    def test_details_test(self):
        """Test all elements from a certain test details query."""
        response = self.client.get(f'/details/test/1')
        self.assertContextEqual(response.context, {
            'item': self.tests[0],
            'type': 'test',
            'results': models.TestResult.objects.all(),
            'result_filter': None,
            'runs': [
                {'pipeline': self.pipelines[1], 'tests': [self.test_runs[2]]},
                {'pipeline': self.pipelines[0], 'tests': [self.test_runs[0]]},
            ],
        })

    def test_details_test_another(self):
        """Test that getting a different test works."""
        response = self.client.get(f'/details/test/2')
        self.assertContextEqual(response.context, {
            'item': self.tests[1],
        })

    def test_details_tests_result(self):
        """Test filtering by results."""
        response = self.client.get(f'/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [],
        })

        response = self.client.get(f'/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'pipeline': self.pipelines[1], 'tests': [self.test_runs[2]]},
                {'pipeline': self.pipelines[0], 'tests': [self.test_runs[0]]},
            ],
        })

    def test_details_tests_table(self):
        """Test returned table."""
        response = self.client.get(f'/details/test/1')
        self.assertContextEqual(response.context, {
            'table': [
                self.hosts[0],
                self.hosts[1],
            ],
        })

        self.assertEqual(response.context['table'][0].total_runs, 1)
        self.assertEqual(response.context['table'][0].FAIL, 0)
        self.assertEqual(response.context['table'][0].PASS, 1)

        self.assertEqual(response.context['table'][1].total_runs, 1)
        self.assertEqual(response.context['table'][1].FAIL, 0)
        self.assertEqual(response.context['table'][1].PASS, 1)

    def test_details_tests_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get(f'/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': [
            ],
        })

        response = self.client.get(f'/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': [
                self.hosts[0],
                self.hosts[1],
            ],
        })

    def test_details_host(self):
        """Test all elements from a certain host details query."""
        response = self.client.get(f'/details/host/1')
        self.assertContextEqual(response.context, {
            'item': self.hosts[0],
            'type': 'host',
            'results': models.TestResult.objects.all(),
            'result_filter': None,
            'runs': [
                {'pipeline': self.pipelines[1], 'tests': [self.test_runs[2]]},
                {'pipeline': self.pipelines[0], 'tests': [self.test_runs[1]]},
            ],
        })

    def test_details_host_another(self):
        """Test that getting a different host works."""
        response = self.client.get(f'/details/host/2')
        self.assertContextEqual(response.context, {
            'item': self.hosts[1],
        })

    def test_details_hosts_result(self):
        """Test filtering by results."""
        response = self.client.get(f'/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'pipeline': self.pipelines[0], 'tests': [self.test_runs[1]]},
            ],
        })

        response = self.client.get(f'/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'pipeline': self.pipelines[1], 'tests': [self.test_runs[2]]},
            ],
        })

    def test_details_hosts_table(self):
        """Test returned table."""
        response = self.client.get(f'/details/host/1')
        self.assertContextEqual(response.context, {
            'table': [
                self.tests[0],
                self.tests[1]
            ],
        })

        self.assertEqual(response.context['table'][0].total_runs, 1)
        self.assertEqual(response.context['table'][0].FAIL, 0)
        self.assertEqual(response.context['table'][0].PASS, 1)

        self.assertEqual(response.context['table'][1].total_runs, 1)
        self.assertEqual(response.context['table'][1].FAIL, 1)
        self.assertEqual(response.context['table'][1].PASS, 0)

    def test_details_hosts_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get(f'/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': [
                self.tests[1]
            ],
        })

        response = self.client.get(f'/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': [
                self.tests[0]
            ],
        })

    def test_issue_new(self):
        """Test creating new issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        response = self.client.post('/issue/new', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                                   'kind_id': issue_kind.id})
        self.assertEqual(302, response.status_code)

        # Check that it was created ok.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual('foo bar', issue.description)
        self.assertEqual(issue_kind, issue.kind)

        # Same ticket_url. Fail.
        response = self.client.post('/issue/new', {'description': 'bar bar', 'ticket_url': 'http://some.url',
                                                   'kind_id': issue_kind.id})
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Issue already exists with ticket URL http://some.url', response.content)

        # Another ticket_url. Ok
        response = self.client.post('/issue/new', {'description': 'bar bar', 'ticket_url': 'http://other.url',
                                                   'kind_id': issue_kind.id})
        self.assertEqual(302, response.status_code)

    def test_issue_edit(self):
        """Test editing issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue_kind_2 = models.IssueKind.objects.create(description="fail 2", tag="2")
        response = self.client.post('/issue/new', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                                   'kind_id': issue_kind.id})
        self.assertEqual(302, response.status_code)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        response = self.client.post('/issue/edit', {'issue_id': issue.id, 'description': 'var var',
                                                    'ticket_url': 'http://other.url', 'kind_id': issue_kind_2.id})
        self.assertEqual(302, response.status_code)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(issue.description, 'var var')
        self.assertEqual(issue.ticket_url, 'http://other.url')
        self.assertEqual(issue.kind, issue_kind_2)

    def test_issue_edit_url_check(self):
        """Test when editing issues it's not possible to duplicate the URL."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.client.post('/issue/new', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                        'kind_id': issue_kind.id})
        self.client.post('/issue/new', {'description': 'foo bar', 'ticket_url': 'http://other.url',
                                        'kind_id': issue_kind.id})

        # Change the values. The url is already used by the second issue.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        response = self.client.post('/issue/edit', {'issue_id': issue.id, 'description': 'foo bar',
                                                    'ticket_url': 'http://other.url', 'kind_id': issue_kind.id})

        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Issue already exists with ticket URL http://other.url', response.content)

    def test_issue_other(self):
        """Test error when action is not recognized."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        response = self.client.post('/issue/fernet', {'description': 'foo bar', 'ticket_url': 'http://other.url',
                                                      'kind_id': issue_kind.id})
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Heh, no idea how to do fernet.', response.content)

    def test_pipeline_issue(self):
        """Test creating new issue records."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        # Create a issue record with no testruns.
        self.client.post(f'/pipeline/{pipeline.pipeline_id}/issue',
                         {'issue_id': issue.id, 'testrun_ids': []})

        self.assertEqual(1, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual([], list(issue_record.testruns.all()))
        self.assertFalse(issue_record.bot_generated)

        # Create a issue record with some testruns.
        self.client.post(f'/pipeline/{pipeline.pipeline_id}/issue',
                         {'issue_id': issue.id,
                          'testrun_ids': pipeline.test_jobs.values_list('id', flat=True)[:2]})

        self.assertEqual(2, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all()))
        self.assertFalse(issue_record.bot_generated)

    def test_pipeline_issue_delete(self):
        """Test deleting issue records."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()
        issue_record = models.IssueRecord.objects.create(pipeline=pipeline, issue=issue)

        self.assertEqual(1, pipeline.issue_records.count())
        self.client.post(f'/pipeline/{pipeline.pipeline_id}/issue/{issue_record.id}/delete', {})
        self.assertEqual(0, pipeline.issue_records.count())

    def test_pipeline_get(self):
        """Test getting a single pipeline."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()
        issue_record = models.IssueRecord.objects.create(pipeline=pipeline, issue=issue)

        response = self.client.get(f'/pipeline/{pipeline.pipeline_id}')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'pipeline': pipeline,
            'test_jobs': [self.test_runs[2], self.test_runs[3]],
            'issues': [issue],
            'issue_kinds': [issue_kind],
            'issue_records': [issue_record],
            'failed_testruns': [self.test_runs[3]]
        })

    def test_dashboard(self):
        """Test dashboard."""
        for index, pipeline in enumerate(models.Pipeline.objects.all()):
            pipeline.created_at = timezone.now() - datetime.timedelta(days=index)
            pipeline.save()

        # First pipeline will be created now - 0d.
        # It's the only one on the last 24hs.
        pipeline = models.Pipeline.objects.first()
        build_fail = models.BuildRun.objects.create(job=self.jobs[1], jid=1, pipeline=pipeline,
                                                    success=False, kernel_arch=self.kernel_arch)
        build_success = models.BuildRun.objects.create(job=self.jobs[2], jid=2, pipeline=pipeline,
                                                       success=True, kernel_arch=self.kernel_arch)

        response = self.client.get(f'/dashboard')
        self.assertEqual(200, response.status_code)

        self.assertContextEqual(response.context, {
            'pipelines': models.Pipeline.objects.all(),
            'pipelines_last_24_hs': [pipeline],
            'build_last_24_hs': [build_fail, build_success],
            'build_last_24_hs_fail': [build_fail],
            'build_last_24_hs_success': [build_success],
            'test_last_24_hs': [self.test_runs[2], self.test_runs[3]],
            'test_last_24_hs_fail': [self.test_runs[3]],
            'test_last_24_hs_success': [self.test_runs[2]],
            'git_trees': [self.git_trees[1]],
        })

    def test_running(self):
        """Test running pipelines view."""
        for pipeline in models.Pipeline.objects.all():
            pipeline.created_at = timezone.now()
            pipeline.duration = None
            pipeline.save()

        response = self.client.get(f'/pipeline/running')
        self.assertEqual(200, response.status_code)

        self.assertContextEqual(response.context, {
            'pipelines': models.Pipeline.objects.all(),
        })

    def test_summary(self):
        """Test pipelines summary view."""
        response = self.client.get(f'/pipeline/summary', follow=True)
        self.assertEqual(200, response.status_code)

        self.assertContextEqual(response.context, {
            'pipelines': models.Pipeline.objects.all(),
            'trees': models.GitTree.objects.all(),
            'selected_tree': 'all',
        })

    def test_summary_filtered(self):
        """Test pipelines summary view with filter."""
        response = self.client.get(f'/pipeline/summary/Tree 1')
        self.assertEqual(200, response.status_code)

        self.assertContextEqual(response.context, {
            'pipelines': models.Pipeline.objects.filter(gittree__name='Tree 1'),
            'trees': models.GitTree.objects.all(),
            'selected_tree': 'Tree 1',
        })

    def test_summary_filtered_not_exists(self):
        """Test pipelines summary view with wrong filter."""
        response = self.client.get(f'/pipeline/summary/Tree X')
        self.assertEqual(200, response.status_code)

        self.assertContextEqual(response.context, {
            'pipelines': [],
            'trees': models.GitTree.objects.all(),
            'selected_tree': 'Tree X',
        })


class IssueViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Unit tests for the issues views."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project),
            models.Pipeline.objects.create(pipeline_id=3, project=self.project),
            models.Pipeline.objects.create(pipeline_id=4, project=self.project),
        ]
        self.host = models.BeakerResource.objects.create(id=1, fqdn='host_1')
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.test = models.Test.objects.create(id=1, name='test_1')
        self.beaker_status = models.BeakerStatus.objects.create(name='Completed')
        self.beaker_result = models.BeakerResult.objects.create(name='Fail')
        self.stages = [
            models.Stage.objects.create(name='lint'),
            models.Stage.objects.create(name='merge'),
            models.Stage.objects.create(name='build'),
            models.Stage.objects.create(name='test'),
        ]
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stages[0]),
            models.Job.objects.create(name='job_2', stage=self.stages[1]),
            models.Job.objects.create(name='job_3', stage=self.stages[2]),
            models.Job.objects.create(name='job_4', stage=self.stages[3]),
        ]

        # Pipeline 1 failed lint
        models.LintRun.objects.create(job=self.jobs[0], jid=0, command='', pipeline=self.pipelines[0], success=False)
        # Pipeline 2 failed merge
        models.MergeRun.objects.create(job=self.jobs[1], jid=1, pipeline=self.pipelines[1], success=False)
        # Pipeline 3 failed build
        models.BuildRun.objects.create(job=self.jobs[2], jid=2, pipeline=self.pipelines[2],
                                       success=False, kernel_arch=self.kernel_arch)
        # Pipeline 4 failed test
        models.BeakerTestRun.objects.create(
            job=self.jobs[3], jid=3, task_id=3, recipe_id=3, pipeline=self.pipelines[3],
            test=self.test, beaker_result=self.beaker_result, beaker_status=self.beaker_status,
            waived=False, kernel_arch=self.kernel_arch, beaker_resource=self.host, retcode=0
        )

        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.issue = models.Issue.objects.create(description='foo', ticket_url='http://url', kind=issue_kind)
        models.IssueRecord.objects.create(pipeline=self.pipelines[0], issue=self.issue)

    def test_failures_all(self):
        """Test failures all stages."""
        response = self.client.get(f'/pipeline/failures/all')
        self.assertContextEqual(response.context, {
            'stages': models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test']),
            'group': 'all',
            'pipelines': models.Pipeline.objects.all(),
        })

    def test_failures_lint(self):
        """Test failures lint."""
        response = self.client.get(f'/pipeline/failures/lint')
        self.assertContextEqual(response.context, {
            'stages': models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test']),
            'group': 'lint',
            'pipelines': [self.pipelines[0]],
        })

    def test_failures_merge(self):
        """Test failures merge."""
        response = self.client.get(f'/pipeline/failures/merge')
        self.assertContextEqual(response.context, {
            'stages': models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test']),
            'group': 'merge',
            'pipelines': [self.pipelines[1]],
        })

    def test_failures_build(self):
        """Test failures build."""
        response = self.client.get(f'/pipeline/failures/build')
        self.assertContextEqual(response.context, {
            'stages': models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test']),
            'group': 'build',
            'pipelines': [self.pipelines[2]],
        })

    def test_failures_test(self):
        """Test failures test."""
        response = self.client.get(f'/pipeline/failures/test')
        self.assertEqual(self.pipelines[3].test_jobs.all()[0].passed, False)
        self.assertContextEqual(response.context, {
            'stages': models.Stage.objects.filter(name__in=['lint', 'merge', 'build', 'test']),
            'group': 'test',
            'pipelines': [self.pipelines[3]],
        })

    def test_issues_view(self):
        """Test issues view."""
        response = self.client.get('/issue/list/unresolved')
        self.assertContextEqual(response.context, {
            'issues': models.Issue.objects.all(),
        })
        response = self.client.get('/issue/list/resolved')
        self.assertContextEqual(response.context, {
            'issues': models.Issue.objects.none(),
        })

        response = self.client.post('/issue/resolve', {'issue_id': self.issue.id, 'resolved': 'on'})

        response = self.client.get('/issue/list/unresolved')
        self.assertContextEqual(response.context, {
            'issues': models.Issue.objects.none(),
        })
        response = self.client.get('/issue/list/resolved')
        self.assertContextEqual(response.context, {
            'issues': models.Issue.objects.all(),
        })


class PatchesTestCase(utils.TestCase):
    """Patches test case."""

    def setUp(self):
        """Set up."""
        series = mock_series()
        patches.store_patchwork_series(series)

        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0)
        )

    def test_patch_get_by_submitter_empty(self):
        """Test patch_get_by_submitter with no email."""
        response = self.client.get('/patches/submitter', follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'group': 'tested',
            'emptypage': True
        })

    def test_patch_get_by_submitter(self):
        """Test patch_get_by_submitter."""
        email = 'submitter@email.com'
        submitter = models.PatchworkSubmitter.objects.get(email=email)

        # Test with no pipelines on the series.
        response = self.client.get(f'/patches/submitter/tested?search={email}')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': [],
            'group': 'tested',
            'search': email,
        })
        response = self.client.get(f'/patches/submitter/skipped?search={email}')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': [],
            'group': 'skipped',
            'search': email,
        })
        response = self.client.get(f'/patches/submitter/missing?search={email}')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': models.PatchworkSeries.objects.all(),
            'group': 'missing',
            'search': email,
        })

        # Add a pipeline to the patches.
        pipeline = models.Pipeline.objects.first()
        for patch in models.Patch.objects.all():
            patch.pipelines.add(pipeline)

        # Test with pipelines.
        response = self.client.get(f'/patches/submitter?search={email}', follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': submitter.patch_series.all(),
            'group': 'tested',
            'search': email,
        })

    def test_patch_get_by_submitter_partial(self):
        """Test patch_get_by_submitter with partial email."""
        response = self.client.get('/patches/submitter?search=submitter', follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'search': 'submitter@email.com',
        })

    def test_patch_get_by_submitter_not_found(self):
        """Test patch_get_by_submitter with inexistant email."""
        response = self.client.get('/patches/submitter?search=fernet', follow=True)
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'Submitter not found', response.content)

    def test_patch_summary_missed(self):
        """Test patch_summary with series missed."""
        response = self.client.get('/patches/summary/missed', follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': models.PatchworkSeries.objects.all(),
            'search': '',
        })

    def test_patch_summary_skipped(self):
        """Test patch_summary with series skipped."""
        series = models.PatchworkSeries.objects.first()
        series.skipped = True
        series.save()

        response = self.client.get('/patches/summary/tested')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': [],
            'search': '',
        })
        response = self.client.get('/patches/summary/skipped')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': models.PatchworkSeries.objects.all(),
            'search': '',
        })
        response = self.client.get('/patches/summary/missed')
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': [],
            'search': '',
        })

    def test_patch_summary_tested(self):
        """Test patch_summary."""
        submitter = models.PatchworkSubmitter.objects.get(email='submitter@email.com')

        # Add a pipeline to the patches.
        pipeline = models.Pipeline.objects.first()
        for patch in models.Patch.objects.all():
            patch.pipelines.add(pipeline)

        response = self.client.get('/patches', follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContextEqual(response.context, {
            'patchseries': submitter.patch_series.all(),
            'search': '',
        })
