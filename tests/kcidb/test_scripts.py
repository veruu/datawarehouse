"""Test the views module."""
import tempfile
import shutil
from django.core.files.base import ContentFile
import kcidb

from tests import utils
from datawarehouse import models
from datawarehouse.utils import timestamp_to_datetime as ttd


def create_pipeline_trigger_variables(pipeline, **kwargs):
    """
    Create a pipeline's trigger variable instance.

    Args:
        pipeline:   Pipeline object.
        **kwargs:   Dictionary of the variables and their values to be
                    created.

    Returns:
        A list of created trigger variable objects.

    """
    return [
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key=key, value=value
        ) for key, value in kwargs.items()
    ]


def add_patches(pipeline):
    """Add Patch to a pipeline fron the variables."""
    for url in pipeline.trigger_variables['patch_urls'].split(' '):
        patch, _ = models.Patch.objects.get_or_create(url=url, subject=url)
        pipeline.patches.add(patch)


class FetchTestCase(utils.TestCase):
    """General fetch test case."""

    def setUp(self):
        """Set up tests."""
        # Let's make it work now, refactor later
        # pylint: disable=too-many-locals,too-many-statements
        self.maxDiff = None  # pylint: disable=invalid-name
        self.max_pipeline_id = 0
        self.max_buildrun_id = 0
        self.max_testrun_id = 0

        def create_get_next_id():
            """Make a function returning a sequence of IDs."""
            next_id = 0

            def get_next_id():
                nonlocal next_id
                return_id = next_id
                next_id = next_id + 1
                return return_id

            return get_next_id

        get_next_pipeline_id = create_get_next_id()
        get_next_jid = create_get_next_id()
        get_next_recipe_id = create_get_next_id()
        get_next_task_id = create_get_next_id()

        # Common objects
        project = models.Project.objects.create(project_id=0)
        stage_merge = models.Stage.objects.create(name="merge")
        stage_build = models.Stage.objects.create(name="build")
        stage_test = models.Stage.objects.create(name="test")
        arch_x86_64 = models.Architecture.objects.create(name="x86_64")
        arch_aarch64 = models.Architecture.objects.create(name="aarch64")
        arch_ppc64 = models.Architecture.objects.create(name="ppc64")
        beaker_resource1 = \
            models.BeakerResource.objects.create(fqdn="host1.example.com")
        beaker_resource2 = \
            models.BeakerResource.objects.create(fqdn="host2.example.com")
        beakerstatus_completed = \
            models.BeakerStatus.objects.create(name="Completed")
        beakerresult_pass = models.BeakerResult.objects.create(name="Pass")
        beakerresult_fail = models.BeakerResult.objects.create(name="Fail")
        testmaintainer_john_dee = \
            models.TestMaintainer.objects.create(name="John Dee",
                                                 email="john@gov.uk")
        testmaintainer_barbra_streisand = \
            models.TestMaintainer.objects.create(name="Barbra Streisand",
                                                 email="barbra@hollywood.com")
        test_a = models.Test.objects.create(name="Test A")
        test_a.maintainers.add(testmaintainer_john_dee)
        test_b = models.Test.objects.create(name="Test B", universal_id="test_b")
        test_b.maintainers.add(testmaintainer_barbra_streisand)
        test_c = models.Test.objects.create(name="Test C")
        test_c.maintainers.add(testmaintainer_john_dee)
        test_c.maintainers.add(testmaintainer_barbra_streisand)

        gittree = models.GitTree.objects.create(name='upstream-stable')

        # Pipeline without patches
        pipeline_without_patches = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_without_patches.id
        create_pipeline_trigger_variables(
            pipeline_without_patches,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="393771b8e43a3eb4df1d759508f7b74f638eeb50",
            branch="master",
        )

        # Pipeline with patches, with successful merge
        pipeline_with_patches_merged = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_patches_merged.id
        create_pipeline_trigger_variables(
            pipeline_with_patches_merged,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="ae26c7278f1f09d8202b7217b942ccd9815c5d79",
            branch="master",
            patch_urls="https://example.com/p1.mbox "
                       "https://example.com/p2.mbox",
        )
        add_patches(pipeline_with_patches_merged)
        models.MergeRun.objects.create(
            pipeline=pipeline_with_patches_merged,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=True
        )

        # Pipeline with patches, with failed merge
        pipeline_with_patches_not_merged = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_patches_not_merged.id
        create_pipeline_trigger_variables(
            pipeline_with_patches_not_merged,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6",
            branch="master",
            patch_urls="https://example.com/p1.mbox "
                       "https://example.com/p2.mbox",
        )
        add_patches(pipeline_with_patches_not_merged)
        models.MergeRun.objects.create(
            pipeline=pipeline_with_patches_not_merged,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=False
        )

        # Pipeline with three builds, two successful, one failed, and tests
        pipeline_with_three_builds = \
            models.Pipeline.objects.create(pipeline_id=get_next_pipeline_id(),
                                           project=project, gittree=gittree,
                                           started_at=ttd('2019-11-14 00:07'),
                                           created_at=ttd('2019-11-14 00:00'))
        self.max_pipeline_id = pipeline_with_three_builds.id
        create_pipeline_trigger_variables(
            pipeline_with_three_builds,
            git_url="https://git.kernel.org/torvalds/linux.git",
            commit_hash="f95c24cc76be04acac65e38cb4810cb4f6f409b7",
            branch="master",
        )
        models.MergeRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=models.Job.objects.create(name="merge", stage=stage_merge),
            jid=get_next_jid(),
            success=True
        )
        # Builds
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_x86_64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build x86_64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=True,
            duration=321,
            compiler=models.Compiler.objects.create(name="foo 1.2")
        ).id
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_ppc64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build ppc64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=True,
            duration=None
        ).id
        self.max_buildrun_id = models.BuildRun.objects.create(
            pipeline=pipeline_with_three_builds,
            kernel_arch=arch_aarch64,
            make_opts="bzImage",
            kernel_package_url="https://example.com/kernel.tar.bz2",
            job=models.Job.objects.create(name="build aarch64",
                                          stage=stage_build),
            jid=get_next_jid(),
            success=False
        ).id

        # Tests for first build
        job = models.Job.objects.create(name="test x86_64", stage=stage_test)
        jid = get_next_jid()
        recipe_id = get_next_recipe_id()
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_a,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_b,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_x86_64,
            beaker_resource=beaker_resource1,
            task_id=get_next_task_id(),
            test=test_c,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id

        # Tests for second build
        job = models.Job.objects.create(name="test ppc64", stage=stage_test)
        jid = get_next_jid()
        recipe_id = get_next_recipe_id()
        # Passed, non-waived
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_a,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_pass,
            retcode=0,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
            duration=123,
        ).id
        # Failed, waived, no started_at
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_b,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_fail,
            retcode=1,
            waived=True,
            duration=123,
        ).id
        # Failed, non-waived, no duration
        self.max_testrun_id = models.BeakerTestRun.objects.create(
            pipeline=pipeline_with_three_builds,
            job=job,
            jid=jid,
            recipe_id=recipe_id,
            kernel_arch=arch_ppc64,
            beaker_resource=beaker_resource2,
            task_id=get_next_task_id(),
            test=test_c,
            beaker_status=beakerstatus_completed,
            beaker_result=beakerresult_fail,
            retcode=1,
            waived=False,
            started_at=ttd('2019-11-14 00:10'),
        ).id

        self.tmpdir = tempfile.TemporaryDirectory().name
        with self.settings(MEDIA_ROOT=self.tmpdir):
            artifacts = {}
            for name in ['testfile.log', 'kernel.tar.gz', 'kernel.config']:
                artifacts[name] = models.Artifact.objects.create(
                    name=name,
                    pipeline=pipeline_with_three_builds,
                )
                artifacts[name].file.save(name, ContentFile(b'dummy content'))
                artifacts[name].save()

            models.BuildRun.objects.filter(pipeline=pipeline_with_three_builds)\
                  .first().artifacts.add(artifacts['kernel.tar.gz'], artifacts['kernel.config'])
            models.BeakerTestRun.objects.filter(pipeline=pipeline_with_three_builds)\
                  .last().logs.add(artifacts['testfile.log'])
            models.BuildRun.objects.filter(pipeline=pipeline_with_three_builds).update(log=artifacts['testfile.log'])
            models.MergeRun.objects.filter(pipeline=pipeline_with_three_builds).update(log=artifacts['testfile.log'])

        self.expected_result = (
            {
                "version": {"major": 1, "minor": 1},
                "revisions": [
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "393771b8e43a3eb4df1d759508f7b74f638eeb50",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 0},
                        "origin": "redhat",
                        "origin_id":
                            "https://git.kernel.org/torvalds/linux.git"
                            "@393771b8e43a3eb4df1d759508f7b74f638eeb50",
                        "patch_mboxes": [],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 2},
                        "origin": "redhat",
                        "origin_id":
                            "https://git.kernel.org/torvalds/linux.git"
                            "@3eef6c7eeb3ad3638cb6d859b48a564e01c12ca6"
                            "+9e1a8dc4c6a0d43442f4991ee9fdbc5453e1c0a8341bbdb817393278ff00aa83",
                        "patch_mboxes": [
                            {"name": "p1.mbox", "url": "https://example.com/p1.mbox"},
                            {"name": "p2.mbox", "url": "https://example.com/p2.mbox"},
                        ],
                        "valid": False,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "ae26c7278f1f09d8202b7217b942ccd9815c5d79",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 1},
                        "origin": "redhat",
                        "origin_id":
                            "https://git.kernel.org/torvalds/linux.git"
                            "@ae26c7278f1f09d8202b7217b942ccd9815c5d79"
                            "+9e1a8dc4c6a0d43442f4991ee9fdbc5453e1c0a8341bbdb817393278ff00aa83",
                        "patch_mboxes": [
                            {"name": "p1.mbox", "url": "https://example.com/p1.mbox"},
                            {"name": "p2.mbox", "url": "https://example.com/p2.mbox"},
                        ],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                    },
                    {
                        "git_repository_branch": "master",
                        "git_repository_commit_hash": "f95c24cc76be04acac65e38cb4810cb4f6f409b7",
                        "git_repository_url": "https://git.kernel.org/torvalds/linux.git",
                        "misc": {"pipeline_id": 3},
                        "origin": "redhat",
                        "origin_id":
                            "https://git.kernel.org/torvalds/linux.git"
                            "@f95c24cc76be04acac65e38cb4810cb4f6f409b7",
                        "patch_mboxes": [],
                        "valid": True,
                        "contacts": [],
                        'discovery_time': '2019-11-14T00:00:00Z',
                        "log_url": "/staticfiles/3/testfile.log",
                    },
                ],
                "builds": [
                    {
                        'architecture': 'x86_64',
                        'command': 'make bzImage',
                        'compiler': 'foo 1.2',
                        'duration': 321,
                        'misc': {'job_id': 3, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '3',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'https://git.kernel.org/torvalds/linux.git'
                            '@f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': True,
                        'config_name': 'fedora',
                        'config_url': '/staticfiles/3/kernel.config',
                        'output_files': [{'name': 'kernel.tar.gz', 'url': '/staticfiles/3/kernel.tar.gz'}],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': '/staticfiles/3/testfile.log',
                    },
                    {
                        'architecture': 'ppc64',
                        'command': 'make bzImage',
                        'misc': {'job_id': 4, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '4',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'https://git.kernel.org/torvalds/linux.git'
                            '@f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': True,
                        'config_name': 'fedora',
                        'output_files': [],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': '/staticfiles/3/testfile.log',
                    },
                    {
                        'architecture': 'aarch64',
                        'command': 'make bzImage',
                        'misc': {'job_id': 5, 'pipeline_id': 3},
                        'origin': 'redhat',
                        'origin_id': '5',
                        'revision_origin': 'redhat',
                        'revision_origin_id':
                            'https://git.kernel.org/torvalds/linux.git'
                            '@f95c24cc76be04acac65e38cb4810cb4f6f409b7',
                        'valid': False,
                        'config_name': 'fedora',
                        'output_files': [],
                        'start_time': '2019-11-14T00:07:00Z',
                        'log_url': '/staticfiles/3/testfile.log',
                    }
                ],
                "tests": [
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test A',
                        'origin': 'redhat',
                        'origin_id': '0',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test B',
                        'origin': 'redhat',
                        'origin_id': '1',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                        'path': "test_b",
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '3',
                        'description': 'Test C',
                        'origin': 'redhat',
                        'origin_id': '2',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test A',
                        'origin': 'redhat',
                        'origin_id': '3',
                        'status': 'PASS',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'duration': 123,
                        'output_files': [],
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test B',
                        'origin': 'redhat',
                        'origin_id': '4',
                        'status': 'FAIL',
                        'waived': True,
                        'duration': 123,
                        'output_files': [],
                        'path': "test_b",
                    },
                    {
                        'build_origin': 'redhat',
                        'build_origin_id': '4',
                        'description': 'Test C',
                        'origin': 'redhat',
                        'origin_id': '5',
                        'status': 'FAIL',
                        'waived': False,
                        'start_time': '2019-11-14T00:10:00Z',
                        'output_files': [{'name': 'testfile.log', 'url': '/staticfiles/3/testfile.log'}],
                    }
                ]
            },
            self.max_pipeline_id,
            self.max_buildrun_id,
            self.max_testrun_id,
        )

    def tearDown(self):
        """TearDown."""
        shutil.rmtree(self.tmpdir)

    def test_revisions(self):
        """Test the revision endpoint."""
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['revisions'].sort(key=lambda x: x['misc']["pipeline_id"])

        self.assertListEqual(
            self.expected_result[0]['revisions'],
            data['revisions']
        )

        kcidb.io_schema.validate(data)

    def test_builds(self):
        """Test the builds endpoint."""
        response = self.client.get(f'/kcidb/1/data/builds?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['builds'].sort(key=lambda x: x["origin_id"])

        self.assertListEqual(
            self.expected_result[0]['builds'],
            data['builds']
        )

        kcidb.io_schema.validate(data)

    def test_tests(self):
        """Test the tests endpoint."""
        response = self.client.get(f'/kcidb/1/data/tests?last_retrieved_id=-1')
        data = response.json()['results']['data']

        self.expected_result[0]['tests'].sort(key=lambda x: x["origin_id"])

        self.assertListEqual(
            self.expected_result[0]['tests'],
            data['tests']
        )

        kcidb.io_schema.validate(data)

    def test_pagination(self):
        """Test the endpoint pagination."""

        # a: [0] 1 2
        response_a = self.client.get(f'/kcidb/1/data/tests?last_retrieved_id=-1&limit=1&offset=0')
        self.assertEqual(1, len(response_a.json()['results']['data']['tests']))

        # b: [0 1] 2
        response_b = self.client.get(f'/kcidb/1/data/tests?last_retrieved_id=-1&limit=2&offset=0')
        self.assertEqual(2, len(response_b.json()['results']['data']['tests']))

        # c: 0 [1 2]
        response_c = self.client.get(f'/kcidb/1/data/tests?last_retrieved_id=-1&limit=2&offset=1')
        self.assertEqual(2, len(response_c.json()['results']['data']['tests']))

        # a.0 == b.0
        self.assertEqual(
            response_a.json()['results']['data']['tests'][0],
            response_b.json()['results']['data']['tests'][0],
            )

        # b.1 == c.0
        self.assertEqual(
            response_b.json()['results']['data']['tests'][1],
            response_c.json()['results']['data']['tests'][0],
            )

    def test_date_filter(self):
        """Test the endpoint min_pipeline_started_at filter."""

        dates = [
            ttd("2000-02-02 04:00:00"),
            ttd("2000-02-02 03:00:00"),
            ttd("2000-02-02 02:00:00"),
            ttd("2000-02-02 01:00:00"),
        ]

        for index, pipeline in enumerate(models.Pipeline.objects.all()):
            pipeline.started_at = dates[index]
            pipeline.save()

        # No time filter
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=-1&limit=3')
        self.assertEqual(3, len(response.json()['results']['data']['revisions']))

        # Old date. Should return all 4.
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=-1&'
                                   'min_pipeline_started_at=2000-02-01 02:00')
        self.assertEqual(4, len(response.json()['results']['data']['revisions']))

        # Date between the list. Should return the latest 2.
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=-1&'
                                   'min_pipeline_started_at=2000-02-02 03:00')
        self.assertEqual(2, len(response.json()['results']['data']['revisions']))
        self.assertEqual(
            models.Pipeline.objects.get(started_at=dates[1]).pipeline_id,
            response.json()['results']['data']['revisions'][0]['misc']['pipeline_id']
        )
        self.assertEqual(
            models.Pipeline.objects.get(started_at=dates[0]).pipeline_id,
            response.json()['results']['data']['revisions'][1]['misc']['pipeline_id']
        )

    def test_last_retrived_id(self):
        """Test last_retrieved_id is ok."""
        # Returned the first item.
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=-1&limit=1')
        self.assertEqual(
            models.Pipeline.objects.last().id,
            response.json()['results']['last_retrieved_id'])

        # Nothing newer than 123456 found. Returning 123456.
        response = self.client.get(f'/kcidb/1/data/revisions?last_retrieved_id=123456&limit=1')
        self.assertEqual(123456, response.json()['results']['last_retrieved_id'])
