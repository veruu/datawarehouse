"""Test the views module."""
import json
import datetime

from django.utils import timezone

from tests import utils
from datawarehouse import models, serializers


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_trees = [
            models.GitTree.objects.create(name='Tree 1'),
            models.GitTree.objects.create(name='Tree 2'),
        ]
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project,
                                           gittree=self.git_trees[0], duration=1234),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project,
                                           gittree=self.git_trees[1]),
        ]
        self.hosts = [
            models.BeakerResource.objects.create(id=1, fqdn='host_1'),
            models.BeakerResource.objects.create(id=2, fqdn='host_2'),
        ]
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.tests = [
            models.Test.objects.create(id=1, name='test_1'),
            models.Test.objects.create(id=2, name='test_2'),
        ]
        self.beaker_statuses = [
            models.BeakerStatus.objects.create(name='Completed'),
            models.BeakerStatus.objects.create(name='Aborted'),
        ]
        self.beaker_results = [
            models.BeakerResult.objects.create(name='Fail'),
            models.BeakerResult.objects.create(name='Pass'),
        ]
        self.stage = models.Stage.objects.create(name='stage')
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stage),
            models.Job.objects.create(name='job_2', stage=self.stage),
            models.Job.objects.create(name='job_3', stage=self.stage),
            models.Job.objects.create(name='job_4', stage=self.stage),
            ]

        # pylint: disable=line-too-long
        tests_list = [
            (0, self.jobs[0], self.pipelines[0], self.tests[0], self.beaker_results[1], self.beaker_statuses[0], self.hosts[1]), # noqa
            (1, self.jobs[1], self.pipelines[0], self.tests[1], self.beaker_results[0], self.beaker_statuses[1], self.hosts[0]), # noqa
            (2, self.jobs[2], self.pipelines[1], self.tests[0], self.beaker_results[1], self.beaker_statuses[1], self.hosts[0]), # noqa
            (3, self.jobs[3], self.pipelines[1], self.tests[1], self.beaker_results[0], self.beaker_statuses[0], self.hosts[1]), # noqa
        ]

        self.test_runs = []
        for incremental_id, job, pipeline, test_, beaker_result, beaker_status, host in tests_list:
            test_run = models.BeakerTestRun.objects.create(
                job=job, jid=incremental_id, task_id=incremental_id, recipe_id=incremental_id,
                pipeline=pipeline, test=test_, beaker_result=beaker_result, beaker_status=beaker_status,
                waived=False, kernel_arch=self.kernel_arch, beaker_resource=host, retcode=0
            )
            self.test_runs.append(test_run)

        models.LintRun.objects.create(job=self.jobs[0], jid=0, command='', pipeline=self.pipelines[0], success=False)
        models.MergeRun.objects.create(job=self.jobs[1], jid=1, pipeline=self.pipelines[1], success=False)

    def test_pipeline_issue(self):
        """Test creating new issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        auth_client = self.get_authenticated_client()

        # Create a issue record with no testruns.
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({'issue_id': issue.id, 'testrun_ids': [], 'bot_generated': False}),
            content_type="application/json")

        self.assertEqual(1, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual([], list(issue_record.testruns.all()))
        self.assertFalse(issue_record.bot_generated)

        # Create a issue record with some testruns.
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({
                'issue_id': issue.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json")

        self.assertEqual(2, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all()))
        self.assertTrue(issue_record.bot_generated)

    def test_pipeline_issue_delete(self):
        """Test deleting issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        auth_client = self.get_authenticated_client()

        # Create a issue record with no testruns.
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({'issue_id': issue.id, 'testrun_ids': [], 'bot_generated': False}),
            content_type="application/json")

        self.assertEqual(1, pipeline.issue_records.count())

        # Delete it.
        record_id = pipeline.issue_records.first().id
        auth_client.delete(f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{record_id}',
                           content_type="application/json")

        self.assertEqual(0, pipeline.issue_records.count())

    def test_pipeline_issue_put(self):
        """Test editing issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue_1 = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        issue_2 = models.Issue.objects.create(description='foo foo', ticket_url='http://other.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        auth_client = self.get_authenticated_client()

        # Create a issue record with some testruns.
        auth_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({
                'issue_id': issue_1.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json")

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all())
        )

        # Change issue kind.
        auth_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({'issue_id': issue_2.id}), content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue_2, issue_record.issue)

        # Change bot_generated.
        auth_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({'bot_generated': False}), content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertFalse(issue_record.bot_generated)

        # Change testruns.
        auth_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:1]),
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            list(pipeline.test_jobs.all())[:1],
            list(issue_record.testruns.all())
        )

        # No testruns at all.
        auth_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'testrun_ids': [],
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            [],
            list(issue_record.testruns.all())
        )

        # Change all at once.
        auth_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'issue_id': issue_1.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue_1, issue_record.issue)
        self.assertTrue(issue_record.bot_generated)
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all())
        )

    def test_pipeline_get_json_not_found(self):
        """Test get_pipeline json. Pipeline not found."""
        response = self.client.get(f'/api/1/pipeline/0')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_pipeline_get_json(self):
        """Test get_pipeline json."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineSerializer(pipeline).data,
            response.json()
        )

    def test_pipeline_get_json_reporter(self):
        """Test get_pipeline json. Reporter style."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}?style=reporter')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineOrderedSerializer(pipeline).data,
            response.json()
        )

    def test_failures_all(self):
        """Test failures all stages."""
        response = self.client.get('/api/1/pipeline/failures/all')
        self.assertEqual(
            response.json()['results'],
            {
                'group': 'all',
                'pipelines': serializers.TriagePipelineSerializer(
                    models.Pipeline.objects.exclude(duration=None), many=True).data,
            }
        )

    def test_failures_lint(self):
        """Test failures lint."""
        response = self.client.get('/api/1/pipeline/failures/lint')
        self.assertEqual(
            response.json()['results'],
            {
                'group': 'lint',
                'pipelines': serializers.TriagePipelineSerializer([self.pipelines[0]], many=True).data,
            }
        )

    def test_get_test(self):
        """Test get all tests."""
        response = self.client.get('/api/1/test')
        self.assertEqual(
            response.json()['results'],
            {
                'tests': serializers.TestSerializer(models.Test.objects.all(), many=True).data
            }
        )

    def test_get_single_test(self):
        """Test get single tests."""
        response = self.client.get('/api/1/test/1')
        self.assertEqual(
            response.json(),
            {
                'test': serializers.TestSerializer(models.Test.objects.get(id=1)).data
            }
        )

    def test_get_single_test_404(self):
        """Test get single tests. It doesn't exist."""
        response = self.client.get('/api/1/test/1234')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    @staticmethod
    def _configure_test_issue_records():
        """Configure issue records for the test."""
        test = models.Test.objects.first()
        testrun = test.testrun_set.first()

        # Create a issue for that test.
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        issue_record = models.IssueRecord.objects.create(issue=issue, pipeline=testrun.pipeline)
        issue_record.testruns.add(testrun)

        # Set up pipeline's created_at to now-1h
        now = timezone.now()
        testrun.pipeline.created_at = now - datetime.timedelta(hours=1)
        testrun.pipeline.save()

        return test

    def test_get_test_issue_records(self):
        """Test get a test issue records."""
        test = self._configure_test_issue_records()

        response = self.client.get(f'/api/1/test/{test.id}/issue/record')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': serializers.IssueRecordSerializer(
                    models.IssueRecord.objects.filter(testruns__test=test),
                    many=True
                ).data
            }
        )

    def test_get_test_issue_records_since_2_hrs(self):
        """Test get a test issue records. Since 2 hours ago."""
        test = self._configure_test_issue_records()
        since_2_hours = datetime.datetime.now() - datetime.timedelta(hours=2)

        response = self.client.get(f'/api/1/test/{test.id}/issue/record?since={since_2_hours}')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': serializers.IssueRecordSerializer(
                    models.IssueRecord.objects.filter(testruns__test=test),
                    many=True
                ).data
            }
        )

    def test_get_test_issue_records_since_now(self):
        """Test get a test issue records. Since now."""
        test = self._configure_test_issue_records()
        now = datetime.datetime.now()

        response = self.client.get(f'/api/1/test/{test.id}/issue/record?since={now}')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': []
            }
        )

    def test_get_records_for_issue(self):
        """Test get records of an issue."""
        self._configure_test_issue_records()

        response = self.client.get(f'/api/1/issue/1/record')
        self.assertEqual(
            response.json()['results'],
            {
                'records': serializers.IssueRecordSerializer(
                    models.Issue.objects.get(id=1).issue_records.all(),
                    many=True
                ).data
            }
        )

    def test_get_records_for_issue_404(self):
        """Test get records of an issue that doesn't exist."""
        response = self.client.get('/api/1/issue/1234/record')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_get_test_stats(self):
        """Test get stats of a test."""
        self._configure_test_issue_records()

        response = self.client.get(f'/api/1/test/1/stats')
        self.assertEqual(
            response.json(),
            {
                'test': {'fetch_url': None, 'id': 1, 'maintainers': [], 'name': 'test_1', 'universal_id': None},
                'issues': {'1': {'rate': 0.5, 'reports': 1}},
                'total_failures': 0,
                'total_runs': 2
            }
        )

    def test_get_test_stats_404(self):
        """Test get stats of a test."""
        response = self.client.get(f'/api/1/test/1234/stats')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)
