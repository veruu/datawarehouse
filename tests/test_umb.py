"""Test the umb module."""
from dataclasses import dataclass
import tempfile
from unittest import mock
from unittest.mock import patch, call
import responses

from django import test
from datawarehouse import umb, models


@dataclass
class Variable:
    """Mock of a Pipeline variable."""

    key: str
    value: str


class UMBTestCase(test.TestCase):
    """Unit tests for the umb module."""

    def setUp(self):
        """Set up."""
        self.project = models.Project.objects.create(project_id=0)
        self.pipeline = models.Pipeline.objects.create(pipeline_id=1, project=self.project)

    @responses.activate
    @patch('datawarehouse.umb.create_test_run')
    def test_process_umb_result(self, create_test_run):
        """Test process_umb_result."""
        mocked_gitlab = mock.Mock()
        mocked_gitlab.variables.list.return_value = [
            Variable('original_pipeline_id', '1'),
            Variable('data', '/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4AULAQZdAC2ewEdDZH3xj2PTABgfa'
                             'WKhStSMn+XoLs43d7wq3ZUQkbA11P6AMZ7L+kACKkmJGh9pigfqZajdn/BQET'
                             'MOcj/Xmaz2XAXKhDxynlUdUuP56iEyW+j5KmqBp0eeTDr4/2Q7/W1XKtvRFGV'
                             'lWoBbkQNXKng3bBKnJfydRUKcrc49Dw12r7BNYu09zzVTXdB+AYyGYTo20IsZ'
                             '4/pvV6P+NoKlmz5N15W68rGIRNC7NHz/FXbgVBfkQuquNlsYbAhnXFMi0wzPe'
                             'SqZ2uqPV93UJLRBQoe9X/loCXsX18x26+8SPuJteZMyiCW1ct8r7xx0iicsrV'
                             'X/9/OTfKZ4p2uX6/V6eM4digAAAAAAeXFxNz90vAABogKMCgAADbtZR7HEZ/s'
                             'CAAAAAARZWg=='),
        ]

        umb.process_umb_result(mocked_gitlab)

        create_test_run.assert_has_calls([
            call(self.pipeline, {'test_name': 'SimpleNetworkRecipe',
                                 'test_description': 'Network performance measurement of {tcp_stream} '
                                                     'x {ipv4} for SimpleNetworkRecipe scenario',
                                 'test_result': 'PASS',
                                 'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                                 'tasks/101037634/logs/taskout.log',
                                 'test_arch': 'x86',
                                 'test_waived': 'True'}),
            call(self.pipeline, {'test_name': 'SimpleNetworkRecipe',
                                 'test_description': 'Network performance measurement of {tcp_stream} '
                                                     'x {ipv6} for SimpleNetworkRecipe scenario',
                                 'test_result': 'FAIL',
                                 'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                                 'tasks/101037635/logs/taskout.log',
                                 'test_arch': 'x86',
                                 'test_waived': 'True'}),
            call(self.pipeline, {'test_name': 'SimpleNetworkRecipe',
                                 'test_description': 'Network performance measurement of {udp_stream} '
                                                     'x {ipv4} for SimpleNetworkRecipe scenario',
                                 'test_result': 'FAIL',
                                 'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                                 'tasks/101037636/logs/taskout.log',
                                 'test_arch': 'x86',
                                 'test_waived': 'True'}),
            call(self.pipeline, {'test_name': 'SimpleNetworkRecipe',
                                 'test_description': 'Network performance measurement of {udp_stream} '
                                                     'x {ipv6} for SimpleNetworkRecipe scenario',
                                 'test_result': 'FAIL',
                                 'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                                 'tasks/101037637/logs/taskout.log',
                                 'test_arch': 'x86',
                                 'test_waived': 'True'})
        ])

    @patch('datawarehouse.umb.download_log_to_testrun')
    def test_create_test_run(self, download_log_to_testrun):
        """Test create_test_run."""
        test_data = {'test_name': 'SimpleNetworkRecipe',
                     'test_description': 'Network performance measurement of {udp_stream} '
                                         'x {ipv6} for SimpleNetworkRecipe scenario',
                     'test_result': 'FAIL',
                     'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                     'tasks/101037637/logs/taskout.log',
                     'test_arch': 'x86',
                     'test_waived': 'True'}

        umb.create_test_run(self.pipeline, test_data)

        test_name = '{test_name}: {test_description}'.format(**test_data)

        test_obj = models.Test.objects.get(name=test_name)
        testrun = models.TestRun.objects.get(test=test_obj)

        self.assertEqual(testrun.pipeline, self.pipeline)
        self.assertEqual(testrun.test, test_obj)
        self.assertEqual(testrun.result.name, 'FAIL')
        self.assertEqual(testrun.waived, True)
        self.assertEqual(testrun.kernel_arch.name, 'x86')
        self.assertEqual(testrun.passed, False)

        self.assertEqual(self.pipeline.test_jobs.first(), testrun)

        download_log_to_testrun.assert_called_with(testrun, test_data['test_log_url'])

    @responses.activate
    def test_download_log_to_testrun(self):
        """Test download_log_to_testrun."""
        responses.add(responses.GET, url='http://logserver.com/taskout.log', body=b'dummy log')
        test_data = {'test_name': 'simplenetworkrecipe',
                     'test_description': 'network performance',
                     'test_result': 'FAIL',
                     'test_log_url': 'http://logserver.com/taskout.log',
                     'test_arch': 'x86',
                     'test_waived': 'True'}

        with tempfile.TemporaryDirectory() as tmpdirname:
            with self.settings(MEDIA_ROOT=tmpdirname):
                umb.create_test_run(self.pipeline, test_data)

                log = models.Artifact.objects.first()
                self.assertEqual(log.name, 'taskout.log')
                self.assertEqual(log.pipeline, self.pipeline)
                self.assertEqual(log.file.read(), b'dummy log')

    @responses.activate
    @patch('time.sleep')
    def test_download_file(self, sleep):
        """Test download_file."""
        responses.add(responses.GET, 'http://fake.url', body=Exception('...'))

        # It tries 3 times waiting.
        self.assertRaises(Exception, umb.download_file, 'http://fake.url')
        self.assertEqual(3, len(list(responses.calls)))
        sleep.assert_has_calls([call(10), call(20)])

        responses.replace(responses.GET, 'http://fake.url', body=b'test')
        sleep.reset_mock()

        # It tries only once and succeeds.
        request = umb.download_file('http://fake.url')
        self.assertEqual(request.content, b'test')
        self.assertFalse(sleep.called)

    def test_validate_schema(self):
        """Test validate_schema."""
        valid_schema = {'test_name': 'SimpleNetworkRecipe',
                        'test_description': 'Network performance measurement of {udp_stream} '
                                            'x {ipv6} for SimpleNetworkRecipe scenario',
                        'test_result': 'FAIL',
                        'test_log_url': 'https://beaker.engineering.redhat.com/recipes/7486926/'
                                        'tasks/101037637/logs/taskout.log',
                        'test_arch': 'x86',
                        'test_waived': 'True'}

        umb.validate_schema(valid_schema)

        data = dict(valid_schema)
        data.pop('test_arch')
        self.assertRaises(Exception, umb.validate_schema, data)

        data = dict(valid_schema)
        data['test_result'] = 'SKIP'
        self.assertRaises(Exception, umb.validate_schema, data)

        data = dict(valid_schema)
        data['test_waived'] = 'yes'
        self.assertRaises(Exception, umb.validate_schema, data)
