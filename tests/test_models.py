"""Test the models module."""

from tests import utils

from datawarehouse import models, scripts


class ModelsTestCase(utils.TestCase):
    """Unit tests for the models module."""

    def setUp(self):
        """Set up tests."""
        self.insertObjects([
            {'model': 'datawarehouse.patch', 'fields': {'url': 'url 1', 'subject': 'subject 1'}},
            {'model': 'datawarehouse.patch', 'fields': {'url': 'url 2', 'subject': 'subject 2'}},
            {'model': 'datawarehouse.patchworkpatch', 'fields': {'patch_ptr': ['url 2', 'subject 2'],
                                                                 'web_url': 'web_url', 'patch_id': 1}},
        ])

    def test_patch_gui_url(self):
        """Test that the GUI URLs are correctly provided by the model."""
        self.assertEqual(models.PatchworkPatch.objects.first().gui_url, 'web_url')
        self.assertEqual(models.Patch.objects.first().gui_url, 'url 1')


class PipelineBeakerStatusTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Unit tests for the test_status results."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.pipeline = models.Pipeline.objects.create(pipeline_id=1, project=self.project)
        self.host = models.BeakerResource.objects.create(id=1, fqdn='host_1')
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.test = models.Test.objects.create(id=1, name='test_1')
        self.statuses = [
            models.BeakerStatus.objects.create(name='Completed'),
            models.BeakerStatus.objects.create(name='Aborted'),
            models.BeakerStatus.objects.create(name='Running'),
            models.BeakerStatus.objects.create(name='Waiting'),
            models.BeakerStatus.objects.create(name='Queued'),
            models.BeakerStatus.objects.create(name='Scheduled'),
            models.BeakerStatus.objects.create(name='New'),
            models.BeakerStatus.objects.create(name='Cancelled'),
        ]
        self.results = [
            models.BeakerResult.objects.create(name='Pass'),
            models.BeakerResult.objects.create(name='Fail'),
            models.BeakerResult.objects.create(name='Warn'),
            models.BeakerResult.objects.create(name='Skip'),
            models.BeakerResult.objects.create(name='Panic'),
            models.BeakerResult.objects.create(name='New'),
        ]

        self.stage = models.Stage.objects.create(name='stage')
        self.job = models.Job.objects.create(name='job_1', stage=self.stage)
        self.test_runs = []

    def _create_testruns(self, testruns):
        """Create testruns database entries."""
        for iid, (result, status, waived) in enumerate(testruns):
            self.test_runs.append(
                models.BeakerTestRun.objects.create(
                    job=self.job, jid=iid, task_id=iid, recipe_id=1, pipeline=self.pipeline, test=self.test,
                    waived=waived, kernel_arch=self.kernel_arch, beaker_resource=self.host, retcode=0,
                    beaker_result=models.BeakerResult.objects.get(name=result),
                    beaker_status=models.BeakerStatus.objects.get(name=status),
                )
            )
        scripts.update_invalid_results(1)

    def test_pipeline_all_pass(self):
        """Test all pass."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Pass', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_all_pass_waived(self):
        """Test all pass even waived."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Pass', 'Completed', True),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_failed(self):
        """Test failed."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Fail', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.assertFalse(self.pipeline.test_passed)
        self.assertFalse(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_failed_waived(self):
        """Test failed but waived."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Fail', 'Completed', True),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_failed_invalid_result(self):
        """Test failed but invalid."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Fail', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.test_runs[1].invalid_result = True
        self.test_runs[1].save()

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_failed_panicked(self):
        """Test panicked."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Panic', 'Aborted', False),
            ('Warn', 'Aborted', False),
            ('Warn', 'Aborted', False),
        ]
        self._create_testruns(tests_list)

        self.assertFalse(self.pipeline.test_passed)
        self.assertFalse(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_failed_panicked_waived(self):
        """Test panicked but waived."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Panic', 'Aborted', True),
            ('Warn', 'Aborted', False),
            ('Warn', 'Aborted', False),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_skipped_success(self):
        """Test skipeed tests."""
        tests_list = [
            ('Pass', 'Completed', False),
            ('Skip', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_skipped_fail(self):
        """Test skipeed tests."""
        tests_list = [
            ('Fail', 'Completed', False),
            ('Skip', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.assertFalse(self.pipeline.test_passed)
        self.assertFalse(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_mixed_things(self):
        """Test skipeed tests."""
        tests_list = [
            ('New', 'Queued', False),
            ('Skip', 'Completed', False),
            ('New', 'Running', False),
            ('New', 'Scheduled', False),
            ('Fail', 'Completed', True),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pipeline_cancelled(self):
        """Test cancelled tests."""
        tests_list = [
            ('Skip', 'Completed', False),
        ]
        self._create_testruns(tests_list)

        self.assertTrue(self.pipeline.test_passed)
        self.assertTrue(models.Pipeline.objects.filter(id=self.pipeline.id).add_stats().first().test_passed)

    def test_pass(self):
        """Test pass."""
        self._create_testruns([('Pass', 'Completed', False)])
        self.assertTrue(self.test_runs[0].passed)
        self.assertEqual('PASS', self.test_runs[0].passed_result)

    def test_skipped(self):
        """Test skipped."""
        self._create_testruns([
            ('Skip', 'Completed', False),
            ('New', 'Running', False),
            ('New', 'Waiting', False),
            ('New', 'Queued', False),
            ('New', 'Scheduled', False),
            ('New', 'New', False),
        ])
        for test_run in self.test_runs:
            self.assertTrue(test_run.passed)
            self.assertEqual('SKIP', test_run.passed_result)

    def test_fail(self):
        """Test fail."""
        self._create_testruns([
            ('Fail', 'Completed', False),
            ('Fail', 'Aborted', False),
            ('Fail', 'Cancelled', False),
        ])
        for test_run in self.test_runs:
            self.assertFalse(test_run.passed)
            self.assertEqual('FAIL', test_run.passed_result)

    def test_fail_waived(self):
        """Test fail waived tests."""
        self._create_testruns([('Fail', 'Completed', True)])
        self.assertFalse(self.test_runs[0].passed)
        self.assertEqual('FAIL', self.test_runs[0].passed_result)
        self.assertTrue(self.test_runs[0].untrusted)

    def test_fail_invalid_result(self):
        """Test fail waived tests."""
        self._create_testruns([('Fail', 'Completed', False)])

        self.test_runs[0].invalid_result = True
        self.test_runs[0].save()

        self.assertFalse(self.test_runs[0].passed)
        self.assertEqual('FAIL', self.test_runs[0].passed_result)
        self.assertTrue(self.test_runs[0].untrusted)

    def test_panic(self):
        """Test panic."""
        self._create_testruns([('Panic', 'Completed', False)])
        self.assertFalse(self.test_runs[0].passed)
        self.assertEqual('ERROR', self.test_runs[0].passed_result)

    def test_testrun_untrusted_on_create(self):
        """
        Test untrusted value is calculated on model creation.

        _create_testruns will create a waived testrun when the True parameter is set.
        """
        self._create_testruns([('Pass', 'Completed', True)])
        test = models.BeakerTestRun.objects.first()

        self.assertTrue(test.untrusted)

    def test_testrun_untrusted_on_save(self):
        """Test untrusted value is updated on every save()."""
        self._create_testruns([('Pass', 'Completed', False)])
        test = models.BeakerTestRun.objects.first()

        self.assertFalse(test.untrusted)

        test_cases = [
            # (waived, invalid_result, expected_untrusted),
            (False, False, False),
            (True, False, True),
            (False, True, True),
            (True, True, True),
        ]

        for waived, invalid_result, expected_untrusted in test_cases:
            test.waived = waived
            test.invalid_result = invalid_result
            test.save()
            self.assertEqual(expected_untrusted, test.untrusted)
