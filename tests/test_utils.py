"""Test the utils module."""

import os
import datetime
import time

from django import test
from django.utils.timezone import make_aware
import responses

from datawarehouse import utils


class UtilsTestCase(test.TestCase):
    """Unit tests for the utils module."""

    TESTS_DIR = os.path.dirname(os.path.abspath(__file__))

    @responses.activate
    def test_parse_patches_from_urls(self):
        """Test the extraction of the patch subject."""
        patch_urls = ['https://server/patch1', 'https://server/patch2']
        subjects = ['panic: ensure preemption is disabled during panic()',
                    'USB: rio500: Remove Rio 500 kernel driver']

        def response_callback(resp):
            """Delay the first GET request to modify the responses order."""
            if resp.url.endswith('patch1'):
                time.sleep(0.1)
            return resp

        with responses.RequestsMock(response_callback=response_callback) as r_mock:
            r_mock.add(responses.GET, url=patch_urls[0],
                       body='From 20bb759a66be52cf4a9ddd17fddaf509e11490cd Mon Sep 17 00:00:00 2001\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       'Date: Sun, 6 Oct 2019 17:58:00 -0700\n' +
                       f'Subject: {subjects[0]}\n' +
                       '\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       '\n' +
                       'commit 20bb759a66be52cf4a9ddd17fddaf509e11490cd upstream.\n')
            r_mock.add(responses.GET, url=patch_urls[1],
                       body='From 015664d15270a112c2371d812f03f7c579b35a73 Mon Sep 17 00:00:00 2001\n' +
                       'From: Other <other@someplace.org>\n' +
                       'Date: Mon, 23 Sep 2019 18:18:43 +0200\n' +
                       f'Subject: {subjects[1]}\n' +
                       '\n' +
                       'From: Other <other@someplace.org>\n' +
                       '\n' +
                       'commit 015664d15270a112c2371d812f03f7c579b35a73 upstream.\n')

            result = utils.parse_patches_from_urls(patch_urls)
            expected = [{'url': url, 'subject': subject} for url, subject in zip(patch_urls, subjects)]
            self.assertEqual(expected, result)

    def test_timestamp_to_datetime(self):
        """Test timestamp_to_datetime."""
        self.assertEqual(None, utils.timestamp_to_datetime(None))

        test_datetime = datetime.datetime.now()
        test_datetime_aware = make_aware(test_datetime)

        self.assertEqual(
            test_datetime_aware,
            utils.timestamp_to_datetime(str(test_datetime))
        )
