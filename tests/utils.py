"""TestCase with utils."""

import json

from django import test
from django.db.models.query import QuerySet
from django.core import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from datawarehouse import models


class TestCase(test.TestCase):
    # pylint: disable=invalid-name
    """TestCase class."""

    def __init__(self, *args, **kwargs):
        """Override Init."""
        super(TestCase, self).__init__(*args, **kwargs)
        self.allow_inheritance = True

    def _resolve_querysets(self, data):
        """Resolve QuerySets."""
        if isinstance(data, dict):
            for key, value in data.items():
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, list):
            for key, value in enumerate(data):
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, QuerySet):
            data = list(data)
        elif hasattr(data, 'object_list'):
            data = self._resolve_querysets(data.object_list)

        if self.allow_inheritance:
            data = self.get_parent_class(data)

        return data

    def assertContextEqual(self, first, second, allow_inheritance=True):
        """
        Assert Context items are equal.

        Second can contain only some of the keys from first.
        """
        self.allow_inheritance = allow_inheritance
        for key in second.keys():
            self.assertEqual(self._resolve_querysets(first[key]), self._resolve_querysets(second[key]), key)

    @staticmethod
    def insertObjects(objects):
        """
        Insert objects into the database.

        Objects are formatted as returned by dumpdata --format json.
        """
        result = []
        for m in serializers.deserialize('json', json.dumps(list(objects))):
            m.save()
            result.append(m.object)
        return result

    @staticmethod
    def get_parent_class(obj):
        """Return the parent class of the object."""
        if isinstance(obj, models.BeakerTestRun):
            return obj.testrun_ptr

        return obj

    @staticmethod
    def get_authenticated_client():
        """Create and return an authenticated client instance."""
        test_user, _ = User.objects.get_or_create(username='test', email='test@test.com')
        token, _ = Token.objects.get_or_create(user=test_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return client
